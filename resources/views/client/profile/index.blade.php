@extends('client.app')

@section('title')
Profile {{ $user->nama_client }}
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 mt-5 mb-5">
            <div class="card">
                <div class="card-body">
                    <h3 class="">{{ $user->nama_client }}</h3>
         
                    <div class="inner-service">
                        <br>                                                      
                        <div class="row">                        
                            
                            <div class="col-md-12">
                                <!-- ALERT -->
                                @include('client.flash')
                                <table class="table">
                                    <tr>
                                        <td>Email</td>
                                        <td>:</td>
                                        <td>{{ $user->email }}</td>
                                    </tr>
                                    
                                    <tr>
                                        <td>No Telp</td>
                                        <td>:</td>
                                        <td>{{ $user->no_telp }}</td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td>{{ $user->alamat }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="text-right"><a href="{{ route('client.edit') }}" class="btn btn-outline-info btn-sm">Edit Profile</a></td>                                        
                                    </tr>
                                </table>
                            </div>

                        </div>
                                                                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection