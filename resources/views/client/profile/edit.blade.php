@extends('client.app')

@section('title')
Edit Profile {{ $user->nama_client }}
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 mt-5 mb-5">
            <div class="card">
                <div class="card-body">
                    <h3 class="">Edit Profile {{ $user->nama_client }}</h3>
         
                    <div class="inner-service">
                        <br>                                                      
                        <div class="row">                     
                            <div class="col-md-12">
                                <form action="{{ route('client.update') }}" method="post">
                                    @csrf 
                                    @method('put')
                                    <div class="form-group">
                                        <label for="">Nama Lengkap</label>
                                        <input type="text" class="form-control" name="nama_client" value="{{ $user->nama_client }}">
                                        @error('nama_client')
                                            <div class="text-danger">
                                                <strong>{{ $message }}</strong>
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">Email</label>
                                        <input type="text" class="form-control" name="email" value="{{ $user->email }}">
                                        @error('email')
                                            <div class="text-danger">
                                                <strong>{{ $message }}</strong>
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">No Telp</label>
                                        <input type="text" class="form-control" name="no_telp" value="{{ $user->no_telp }}">
                                        @error('no_telp')
                                            <div class="text-danger">
                                                <strong>{{ $message }}</strong>
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="">Alamat</label>
                                        <textarea  class="form-control" name="alamat">{{ $user->alamat }}</textarea>
                                        @error('alamat')
                                            <div class="text-danger">
                                                <strong>{{ $message }}</strong>
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-block btn-success" value="Simpan">
                                    </div>
                                </form>
                             
                                
                            </div>

                        </div>
                                                                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection