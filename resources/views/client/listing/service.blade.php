@extends('client.app')

@section('title')
{{ $category->nama_kategori }}
@endsection

@section('content')
@include('order.modal')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-5 mb-5">
            @include('client.flash')
            <div class="card">
                <div class="card-body">
                    <h3 class=" text-center">{{ $category->nama_kategori }}</h3>
                    <hr>
                    <div class="inner-service">                        
                        <br>                                                      
                        <div class="row">
                            @foreach($services as $service)  
                                <div class="col-md-3 mb-3">
                                    <div class="card card-bordered">
                                        @if($service->thumbnail == null)
                                        <img class="card-img-top img-fluid" src="{{ asset('img/tidak-ada-foto.jpg') }}" alt="Tidak ada foto">
                                        @endif
                                        <img class="card-img-top img-fluid" src="{{ asset('storage/service_thumbnail/small/'. $service->thumbnail ) }}" alt="{{ $service->thumbnail }}">
                                        <div class="card-body">
                                            <div class="equalize-heights">
                                                <h5 class="title">{{ $service->nama_service }}</h5>
                                                <p class="card-text price-text">Rp. {{ number_format($service->harga) }}</p>
                                            </div>
                                            <ul class="btn-card text-center">
                                                <li>
                                                    <a href="{{ route('layanan.detail', ['slug_kategori' => $category->slug, 'slug_service' => $service->slug]) }}" class="btn btn-primary btn-sm">Lihat Detail</a> 
                                                </li>
                                                    @if (Auth::guard('client')->check())
                                                        @if (!userHasUnPaidOrder(Auth::guard('client')->user()))
                                                            <li>
                                                                <a href="#" class="btn btn-outline-success btn-sm place-order" data-service="{{ $service->id }}">Pesan Sekarang</a>                                        
                                                            </li>
                                                        @endif

                                                        @if (userHasUnPaidOrder(Auth::guard('client')->user()))
                                                            <li>
                                                                <form action="{{ route('order.add', Auth::guard('client')->user()->unpaidOrder()->id) }}" method="post">
                                                                    @csrf
                                                                    <input type="hidden" name="service_id" value="{{ $service->id }}">
                                                                    <button type="submit" class="btn btn-outline-success btn-sm">Tambah ke order</button>
                                                                </form>
                                                            </li>
                                                        @endif
                                                    @else 
                                                        <li>
                                                            <a href="{{ route('client.login') }}" class="btn btn-outline-success btn-sm">Pesan </a>                                        
                                                        </li>

                                                    @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>  
                            @endforeach              
                        </div>
                        <div class="row">
                            <div class="col-md-12 my-pagination">                            
                                @if ($services instanceof \Illuminate\Pagination\LengthAwarePaginator )
                                    {{ $services->links() }}
                                @endif
                            </div>
                        </div>                                                   
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    (function($) {
        $('.place-order').on('click', function(e) {
            e.preventDefault();
            $('#orderModal').modal('show');
            $('input[name="service_id"]').val($(this).data('service'));
        });
    })(jQuery)
    </script> 
@endpush