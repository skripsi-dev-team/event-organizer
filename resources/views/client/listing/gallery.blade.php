@extends('client.app')

@section('title')
Gallery
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-5 mb-5">
            @include('client.flash')
            <div class="card">
                <div class="card-body">
                    <h3 class=" text-center">Gallery Foto</h3>
                    <hr>
                    <div class="inner-service">
                        
                        <br>                                                      
                        <div class="row">
                            @foreach($galleries as $data)
                                <div class="col-md-3 mb-5">
                                    <div class="popup-gallery">
                                        <a href="{{ asset('storage/gallery/'. $data->foto ) }}" title="{{ $data->name }}"><img src="{{ asset('storage/gallery/small/'. $data->foto ) }}" class="img-fluid my-img-fluid"></a>
                                    </div>                                                                  
                                                                  
                                </div>  
                            @endforeach              
                        </div>
                        <div class="row">
                            <div class="col-md-12 my-pagination">                            
                                @if ($galleries instanceof \Illuminate\Pagination\LengthAwarePaginator )
                                    {{ $galleries->links() }}
                                @endif
                            </div>
                        </div>                                                   
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
<script src="{{ asset('magnific-popup/dist/jquery.magnific-popup.js') }}"></script>
<script>
$(document).ready(function() {
	$('.popup-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
			titleSrc: function(item) {
				return item.el.attr('title') + '<small>by LaSari Bali Weeding and Event</small>';
			}
		}
	});
});
</script>

@endpush