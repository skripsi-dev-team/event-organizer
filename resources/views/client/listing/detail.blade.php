@extends('client.app')

@section('title')
{{ $service->nama_service }}
@endsection

@section('content')
@include('order.modal')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-5 mb-5">
            @include('client.flash')
            <div class="card">
                <div class="card-body">
                    <h3 class="">Detail Layanan</h3>
                    <hr>
                    <div class="inner-service">
                        <br>                                                      
                        <div class="row">                            
                            <div class="col-md-4">
                                @if($service->thumbnail == null)
                                <img class="card-img-top img-fluid" src="{{ asset('img/tidak-ada-foto.jpg') }}" alt="Tidak ada foto">
                                @endif
                                <img class="card-img-top img-fluid" src="{{ asset('storage/service_thumbnail/'. $service->thumbnail ) }}" alt="{{ $service->thumbnail }}">
                            </div>
                            
                            <div class="col-md-8">
                                <table class="table">
                                    <tr>
                                        <td>Kode</td>
                                        <td>:</td>
                                        <td>{{ $service->kode_service }}</td>
                                    </tr>
                                    <tr>
                                        <td>Nama</td>
                                        <td>:</td>
                                        <td>{{ $service->nama_service }}</td>
                                    </tr>
                                    <tr>
                                        <td>Harga</td>
                                        <td>:</td>
                                        <td>{{ $service->harga }}</td>
                                    </tr>
                                    <tr>
                                        <td>Deskripsi</td>
                                        <td>:</td>
                                        <td>{!! $service->deskripsi !!}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            @if (Auth::guard('client')->check())
                                                @if (!userHasUnPaidOrder(Auth::guard('client')->user()))
                                               
                                                    <a href="#" class="btn btn-outline-success place-order" data-service="{{ $service->id }}">Pesan Sekarang</a>                                        
                                                    
                                                @endif

                                                @if (userHasUnPaidOrder(Auth::guard('client')->user()))                                               
                                                    <form action="{{ route('order.add', Auth::guard('client')->user()->unpaidOrder()->id) }}" method="post">
                                                        @csrf
                                                        <input type="hidden" name="service_id" value="{{ $service->id }}">
                                                        <button type="submit" class="btn btn-outline-success">Tambah ke order</button>
                                                    </form>                                                    
                                                @endif
                                            @else                                                
                                                <a href="{{ route('client.login') }}" class="btn btn-outline-success">Pesan </a>
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                                                                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    (function($) {
        $('.place-order').on('click', function(e) {
            e.preventDefault();
            $('#orderModal').modal('show');
            $('input[name="service_id"]').val($(this).data('service'));
        });
    })(jQuery)
    </script> 
@endpush