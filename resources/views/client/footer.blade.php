        <footer id="contact">
            <div class="footer-area pt-5">
                <div class="footer-contact">
                    <h4>Contact Us</h4>
              
                    <ul class=" footer-detail">
                        <li><i class="fa fa-map-marker"></i> &nbsp; 876 Towne Estate Rosenbaumchester, WI 05642</li>
                        <li><i class="fa fa-phone"></i>&nbsp; Telfon : +62 857 xxx xxx / +62 484 xxx xxx</li>
                        <li><i class="fa fa-envelope"></i>&nbsp; Email : lasaribali@test.com</li>
                    </ul>
                    <h5 class="">Follow Sosial Media Kami</h5>
                    <ul class=" footer-detail footer-icon">
                        <li>
                            <a href="#" target="_blank" class="socmed-icon ">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank" class="socmed-icon ">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>                            
                    </ul>
                </div>
                <p class="footer-tag">© Copyright 2019. All right reserved. by <a href="#">LaSari Bali</a>.</p>
            </div>
        </footer>