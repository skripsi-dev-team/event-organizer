@extends('admin.app')

@section('title')
Login Client
@endsection

@section('content')
<div class="login-area login-bg">
    <div class="container">

        <div class="login-box ptb--100">
            <form method="POST" action="{{ route('client.login') }}">
                <div class="login-form-head">
                    <h4>Login Client</h4>
                    <p>Silahkan login terlebih dahulu!</p>
                </div>
                <div class="login-form-body">
                    
                    <!-- ALERT -->
                    @include('client.flash')

                    @csrf
                    <div class="form-gp">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" id="exampleInputEmail1" name="email" value="{{ old('email') }}">
                        <i class="ti-email"></i>
                        @error('email')
                            <div class="text-danger">
                                <strong>{{ $message }}</strong>
                            </div>
                        @enderror
                    </div>
                    <div class="form-gp">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" id="exampleInputPassword1" name="password" >
                        <i class="ti-lock"></i>
                        @error('password')
                            <span class="text-danger">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="row mb-4 rmber-area">
                        <div class="col-6">
                            <div class="custom-control custom-checkbox mr-sm-2">
                                <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
                                <label class="custom-control-label" for="customControlAutosizing">Remember Me</label>
                            </div>
                        </div>
                        
                    </div>
                    <div class="submit-btn-area">
                        <button id="form_submit" type="submit">Submit <i class="ti-arrow-right"></i></button>                            
                    </div>

                    <div class="form-footer text-center mt-5">
                        <p class="text-muted">Belum punya akun? <a href="{{ route('client.registerform') }}">Daftar disini</a></p>
                    </div>
                
                    
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
