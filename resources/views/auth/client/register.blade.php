@extends('admin.app')

@section('title')
Register Client
@endsection

@section('content')
<div class="login-area login-bg">
    <div class="container">
        <div class="login-box ptb--100">
            <form action="{{ route('client.register') }}" method="POST">
                <div class="login-form-head">
                    <h4>Register Client</h4>
                    <p>Silahkan daftar melalui form dibawah!</p>
                </div>
                <div class="login-form-body">
                    @csrf
                    <div class="form-gp">
                        <label for="nama_client">Nama Lengkap</label>
                        <input type="text" name="nama_client" value="{{ old('nama_client') }}">
                        <i class="ti-user"></i>
                        @error('nama_client')
                            <div class="text-danger">
                                <strong>{{ $message }}</strong>
                            </div>
                        @enderror
                    </div>
                    <div class="form-gp">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" id="exampleInputEmail1" name="email" value="{{ old('email') }}">
                        <i class="ti-email"></i>
                        @error('email')
                            <div class="text-danger">
                                <strong>{{ $message }}</strong>
                            </div>
                        @enderror
                    </div>
                    <div class="form-gp">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" id="exampleInputPassword1" name="password" >
                        <i class="ti-lock"></i>
                        @error('password')
                            <span class="text-danger">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-gp">
                        <label for="password_confirmation">Konfirmasi Password</label>
                        <input type="password" id="password_confirmation" name="password_confirmation" >
                        <i class="ti-lock"></i>
                    </div>
                    <div class="form-gp">
                        <label for="no_telp">No Telp</label>
                        <input type="number" name="no_telp" value="{{ old('no_telp') }}" >
                        <i class="ti-user"></i>
                        @error('no_telp')
                            <div class="text-danger">
                                <strong>{{ $message }}</strong>
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <textarea name="alamat" class="form-control">{{ old('alamat') }}</textarea>
                     
                        @error('alamat')
                            <div class="text-danger">
                                <strong>{{ $message }}</strong>
                            </div>
                        @enderror
                    </div>                    
                 
                    <div class="submit-btn-area">
                        <button id="form_submit" type="submit">Submit <i class="ti-arrow-right"></i></button>                            
                    </div>

                    <div class="form-footer text-center mt-5">
                        <p class="text-muted">Sudah punya akun? <a href="{{ route('client.loginform') }}">Login disini</a></p>
                    </div>
                
                    
                </div>
            </form>
        </div>
    </div>
</div>

@endsection


