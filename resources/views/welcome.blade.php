@extends('client.app')

@section('title')
Selamat Datang
@endsection

@section('content')
@include('order.modal')
<div id="carouselExampleIndicators" class="carousel slide my-slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="{{ asset('img/carousel-1.jpg') }}" class="d-block w-100" alt="...">
            <div class="carousel-caption d-md-block">
                <h5>LaSari Bali Weeding & Event</h5>
                <p>Solusi untuk segala kebutuhan acara anda!</p>
            </div>
        </div>
        <div class="carousel-item">
            <img src="{{ asset('img/carousel-2.jpg') }}" class="d-block w-100" alt="...">
            <div class="carousel-caption d-md-block">
                <h5>Ingin mengadakan acara?</h5>
                <p>Serahkan semua kepada kami, kami siap membantu anda.</p>
            </div>
        </div>
        <div class="carousel-item">
            <img src="{{ asset('img/carousel-3.jpg') }}" class="d-block w-100" alt="...">
            <div class="carousel-caption d-md-block">
                <h5>Jaminan acara sukses</h5>
                <p>Kami pastikan acara anda akan berjalan sesuai harapan.</p>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>


    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-5 mb-5">
                <div class="card">
                    <div class="card-body">

                        <div class="row mb-5 pt-5 pb-5">
                            <div class="col-md-12 mb-5">
                                <h4 class="text-center home-title">Tentang Kami</h4>
                            </div>
                            <div class="col-md-12">
                                <div class="home-section-1">                                    
                                    <div class="home-content text-center">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia dolores officia, temporibus optio asperiores unde culpa. Vero facilis quasi recusandae, corrupti fugiat optio omnis ab delectus sapiente nisi et beatae! Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia dolores officia, temporibus optio asperiores unde culpa. Vero facilis quasi recusandae, corrupti fugiat optio omnis ab delectus sapiente nisi et beatae!</p>
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia dolores officia, temporibus</p>
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia dolores officia, temporibus Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia </p>

                                    </div>
                                </div>
                            </div>
                           
                        </div>

                        <div class="row pt-5 pb-5">
                            <div class="col-md-12">                        
                                <div class="home-section">
                                    <h4 class="text-center home-title">Melayani</h4>
                                    <div class="home-content pt-5 pb-5">
                                        <div class="row text-center justify-content-center row-eq-height">
                                            @foreach($categories as $category)
                                            <div class="col-md-2 eq-col mb-5">
                                                <div class="blurb">
                                                    <a href="{{ route('layanan.service', ['id' => $category->slug]) }}">
                                                        <div class="blurb-title">
                                                            <h4>{{ $category->nama_kategori }}</h4>
                                                        </div>
                                                        <div class="blurb-icon">
                                                            <i class="fa fa-check"></i>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            @endforeach                                                                       
                                        </div>
                                        <div class="row mb-5">
                                            <div class="col-md-12 text-center">
                                            <a href="{{ route('layanan.index') }}" class="btn btn-outline-info btn-lg">Lihat Layanan lain</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                    
                        </div>

                        <div class="row mb-5 pt-5 pb-5">
                            <div class="col-md-12 mb-5">
                                <h4 class="text-center home-title">Paket Kami</h4>
                            </div>
                            @foreach($services as $service)  
                                <div class="col-md-3 mb-3">
                                    <div class="card card-bordered">
                                        @if($service->thumbnail == null)
                                        <img class="card-img-top img-fluid" src="{{ asset('img/tidak-ada-foto.jpg') }}" alt="Tidak ada foto">
                                        @endif
                                        <img class="card-img-top img-fluid" src="{{ asset('storage/service_thumbnail/small/'. $service->thumbnail ) }}" alt="{{ $service->thumbnail }}">
                                        <div class="card-body">
                                            <div class="equalize-heights">
                                                <h5 class="title">{{ $service->nama_service }}</h5>
                                                <p class="card-text price-text">Rp. {{ number_format($service->harga) }}</p>
                                            </div>
                                            <ul class="btn-card text-center">
                                                <li>
                                                    <a href="{{ route('layanan.detail', ['slug_kategori' => $category->slug, 'slug_service' => $service->slug]) }}" class="btn btn-primary btn-sm">Lihat Detail</a> 
                                                </li>
                                                    @if (Auth::guard('client')->check())
                                                        @if (!userHasUnPaidOrder(Auth::guard('client')->user()))
                                                            <li>
                                                                <a href="#" class="btn btn-outline-success btn-sm place-order" data-service="{{ $service->id }}">Pesan Sekarang</a>                                        
                                                            </li>
                                                        @endif

                                                        @if (userHasUnPaidOrder(Auth::guard('client')->user()))
                                                            <li>
                                                                <form action="{{ route('order.add', Auth::guard('client')->user()->unpaidOrder()->id) }}" method="post">
                                                                    @csrf
                                                                    <input type="hidden" name="service_id" value="{{ $service->id }}">
                                                                    <button type="submit" class="btn btn-outline-success btn-sm">Tambah ke order</button>
                                                                </form>
                                                            </li>
                                                        @endif
                                                    @else 
                                                        <li>
                                                            <a href="{{ route('client.login') }}" class="btn btn-outline-success btn-sm">Pesan </a>                                        
                                                        </li>

                                                    @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>  
                            @endforeach 
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
   


@endsection


@push('scripts')
<script>
    (function($) {
        $('.place-order').on('click', function(e) {
            e.preventDefault();
            $('#orderModal').modal('show');
            $('input[name="service_id"]').val($(this).data('service'));
        });
    })(jQuery)
    </script> 
@endpush