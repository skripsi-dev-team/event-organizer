<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title') - LaSari Bali</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="templates/assets/images/icon/favicon.ico">
    <link rel="stylesheet" href="{{ asset('templates/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/assets/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/assets/css/metisMenu.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/assets/css/slicknav.min.css') }}">
    
    <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
    <!-- style css -->
    <link rel="stylesheet" href="{{ asset('templates/assets/css/typography.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/assets/css/default-css.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/assets/css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/assets/css/responsive.css') }}">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{ asset('magnific-popup/dist/magnific-popup.css') }}">

    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!-- modernizr css -->
    <script src="{{ asset('templates/assets/js/vendor/modernizr-2.8.3.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('chartjs/Chart.min.css') }}">

    
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    @guest('web')
        @yield('content')
    @else
    <!-- page container area start -->
    <div class="page-container">
        @include('admin.sidebar')

        <!-- main content area start -->
        <div class="main-content">
            @include('admin.header')
            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Dashboard</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{ url('admin') }}">Home</a></li>
                                @yield('breadcrumb')
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            <h4 class="user-name dropdown-toggle" data-toggle="dropdown">{{ ucwords(Auth::guard('web')->user()->name) }} <i class="fa fa-angle-down"></i></h4>
                            <div class="dropdown-menu">
                                <form action="{{ route('logout') }}" method="post">
                                    @csrf
                                    <button class="dropdown-item" type="submit">Logout</button>
                                </form>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->

            <!-- MAIN CONTENT -->
            @yield('content')

        </div>
        <!-- main content area end -->

        @include('admin.footer')

    </div>
    @endguest
    <!-- offset area end -->
    <!-- jquery latest version -->
    <script src="{{ asset('templates/assets/js/vendor/jquery-2.2.4.min.js') }}"></script>
    <!-- bootstrap 4 js -->
    <script src="{{ asset('templates/assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('templates/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('templates/assets/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('templates/assets/js/metisMenu.min.js') }}"></script>
    <script src="{{ asset('templates/assets/js/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('templates/assets/js/jquery.slicknav.min.js') }}"></script>

    <!-- Start datatable js -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
    <!-- others plugins -->
    <script src="{{ asset('templates/assets/js/plugins.js') }}"></script>
    <script src="{{ asset('templates/assets/js/scripts.js') }}"></script>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('magnific-popup/dist/jquery.magnific-popup.js') }}"></script>
    <script src="{{ asset('chartjs/Chart.min.js') }}"></script>

    <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace( 'myeditor' );
    </script>

    @stack('scripts')
    
</body>

</html>