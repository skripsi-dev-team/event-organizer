@extends('admin.app')

@section('title')
Dashboard
@endsection

@section('breadcrumb')

<li><span>Dashboard</span></li>

@endsection

@section('content')
<div class="main-content-inner">
    <div class="row mt-5 mb-4">
        <div class="col-md-4">
            <div class="seo-fact sbg1">
                <div class="p-4 d-flex justify-content-between align-items-center">
                    <div class="seofct-icon"><i class="ti-briefcase"></i> Total Layanan</div>
                    <h2>{{ $total_service }}</h2>
                </div>                   
            </div>
        </div>
        <div class="col-md-4">
            <div class="seo-fact sbg2">
                <div class="p-4 d-flex justify-content-between align-items-center">
                    <div class="seofct-icon"><i class="ti-shopping-cart"></i> Total Pesanan</div>
                    <h2>{{ $total_order }}</h2>
                </div>                   
            </div>
        </div>
        <div class="col-md-4">
            <div class="seo-fact sbg4">
                <div class="p-4 d-flex justify-content-between align-items-center">
                    <div class="seofct-icon"><i class="ti-user"></i> Total Client</div>
                    <h2>{{ $total_client }}</h2>
                </div>                   
            </div>
        </div>
    </div>
    <div class="row mt-5 mb-4">
        <div class="col-md-4">
            <div class="single-report">
                <div class="s-report-inner pr--20 pt--30 mb-3">
                    <div class="icon"><i class="fa fa-bars"></i></div>
                    <div class="s-report-title d-flex justify-content-between">
                        <h4 class="header-title mb-0">Acara Selesai</h4>                        
                    </div>
                    <div class="d-flex justify-content-between pb-2">
                        <h2 style="font-size:30px">{{ $selesai }}</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="single-report">
                <div class="s-report-inner pr--20 pt--30 mb-3">
                    <div class="icon"><i class="fa fa-bars"></i></div>
                    <div class="s-report-title d-flex justify-content-between">
                        <h4 class="header-title mb-0">Pesanan On Progress</h4>                        
                    </div>
                    <div class="d-flex justify-content-between pb-2">
                        <h2 style="font-size:30px">{{ $progress }}</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="single-report">
                <div class="s-report-inner pr--20 pt--30 mb-3">
                    <div class="icon"><i class="fa fa-bars"></i></div>
                    <div class="s-report-title d-flex justify-content-between">
                        <h4 class="header-title mb-0">Pesanan Belum Dibayar</h4>                        
                    </div>
                    <div class="d-flex justify-content-between pb-2">
                        <h2 style="font-size:30px">{{ $belum_bayar }}</h2>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="row mb-4">
        <div class="col-md-6">
            <div class="card">
                <div class="cart-body">
                    <canvas id="chart1" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="cart-body">
                    <canvas id="chart2" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 ">
            
            @include('admin.flash')

            <div class="card">                
                <div class="card-body">
                    <h4 class="header-title">Selamat Datang</h4>
                    
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure, neque sit. Doloremque nobis facere necessitatibus deleniti natus autem dicta, praesentium impedit obcaecati sunt laboriosam incidunt alias aut veritatis harum commodi!
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure, neque sit. Doloremque nobis facere necessitatibus deleniti natus autem dicta, praesentium impedit obcaecati sunt laboriosam incidunt alias aut veritatis harum commodi!
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure, neque sit. Doloremque nobis facere necessitatibus deleniti natus autem dicta, praesentium impedit obcaecati sunt laboriosam incidunt alias aut veritatis harum commodi!</p>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@push('scripts')
<script>
(function($) {
$.get('/api/chart', function(response) {
    let data = response.data;

    let services = data.services.map(function(item) {
        return item.service
    });
    let services_count = data.services.map(function(item) {
        return item.count;
    });

    let years = data.orders.map(function(item) {
        return item.year;
    });
    let counts = data.orders.map(function(item) {
        return item.count
    })

    var ctx = document.getElementById('chart1');
    var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: services,
        datasets: [{
            label: '# Layanan Terlaris',
            data: services_count,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    // Dummy data
    // Uncomment this to see chart looks like
    // years = [2015, 2016, 2017, 2018, 2019];
    // counts = [20, 50, 12, 15, 20];

    var ctx2 = document.getElementById('chart2');
    var myChart2 = new Chart(ctx2, {
    type: 'line',
    data: {
        labels: years,
        datasets: [{
            label: '# Penjualan 5 Tahun Terakhir',
            data: counts,
            backgroundColor: 'rgba(255, 99, 132, 0.2)',
            borderColor: 'rgba(255, 99, 132, 1)',
            borderWidth: 1,
            fill: false
        }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

});
})(jQuery)
</script>    
@endpush


