@extends('admin.app')

@section('title')
Admin
@endsection

@section('breadcrumb')

<li><span>Admin</span></li>

@endsection

@section('content')
<div class="main-content-inner">
     
    <div class="row">
        <div class="col-12 mt-5">
            
            @include('admin.flash')

            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Data Admin</h4>

                    <a data-toggle="collapse" href="#accordion41" aria-expanded="true" class="btn btn-info btn-sm" onclick="toggleContent()">Tambah Admin</a>
                    <!-- form input -->
                    <div id="accordion4" class="according accordion-s3 gradiant-bg col-md-6 my-form">
                        <div class="card">
                            <div id="accordion41" class="collapse" data-parent="#accordion4" style="">
                                <div class="card-body">
                                    <form action="{{ route('user.store') }}" method="post">
                                        @csrf
                                        <div class="form-group">
                                            <label for="nama_kategori" class="col-form-label">Nama </label>
                                            <input class="form-control" type="text" value="{{ old('name') }}" id="name" name="name">
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="col-form-label">Email </label>
                                            <input class="form-control" type="email" value="{{ old('email') }}" name="email">
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="col-form-label">Password </label>
                                            <input type="password" class="form-control" name="password"  value="">
                        
                                        </div>
                                        <div class="form-group">
                                            <label for="password_confirmation" class="col-form-label">Konfirmasi Password </label>
                                            <input type="password" class="form-control" name="password_confirmation"  value="">
                                            
                                        </div>
                                        <div class="form-group">
                                            <label for="no_telp" class="col-form-label">No Telp </label>
                                            <input class="form-control" type="number" value="{{ old('no_telp') }}" name="no_telp">
                                        </div>
                                        <div class="form-group">
                                            <label for="alamat" class="col-form-label">Alamat</label>
                                            <textarea name="alamat" class="form-control"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-block btn-success" value="Simpan">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <div class="data-tables inner-content">
                        <table id="dataTable" class="text-center">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>No Telp</th>
                                    <th>Alamat</th>
                                    <th>Button</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $row)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $row->name }}</td>
                                    <td>{{ $row->email }}</td>
                                    <td>{{ $row->no_telp }}</td>
                                    <td>{{ $row->alamat }}</td>
                                    
                                    <td>
                                        <form action="{{ route('user.destroy', ['id' => $row->id]) }}" method="post">
                                        <ul class="d-flex justify-content-center">
                                            <li class="mr-3"><a href="{{ route('user.edit', ['id' => $row->id ]) }}" class="text-secondary"><i class="fa fa-edit"></i></a></li>
                                            @csrf
                                            @method('delete')
                                            <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="ti-trash"></i></button></li>
                                        </ul>
                                        </form>
                                    </td>                                  
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>


function toggleContent(){
    if($('.collapse').hasClass('show')){
        $('.inner-content').css('display', 'block');
    } else {
        $('.inner-content').css('display', 'none');
    }
}
</script>
@endpush
