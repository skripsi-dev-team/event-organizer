@extends('admin.app')

@section('title')
Edit Admin
@endsection

@section('breadcrumb')

<li><a href="{{ route('user.index') }}">Admin</a></li>
<li><span>Edit</span></li>

@endsection

@section('content')
<div class="main-content-inner">
     
    <div class="row">
        <div class="col-6 mt-5">
            
            @include('admin.flash')

            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Edit Admin</h4>

                    <div id="accordion4" class="according accordion-s3 gradiant-bg col-md-12 my-form">
                        <div class="card">
                            <div id="accordion41" class="collapse show" data-parent="#accordion4" style="">
                                <div class="card-body">
                                    <form action="{{ route('user.update', ['id' => $admin->id]) }}" method="post">
                                        @csrf
                                        @method('put')
                                        <div class="form-group">
                                            <label for="nama_kategori" class="col-form-label">Nama </label>
                                            <input class="form-control" type="text" value="{{ $admin->name }}" id="name" name="name">
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="col-form-label">Email </label>
                                            <input class="form-control" type="email" value="{{ $admin->email }}" name="email">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="no_telp" class="col-form-label">No Telp </label>
                                            <input class="form-control" type="number" value="{{ $admin->no_telp }}" name="no_telp">
                                        </div>
                                        <div class="form-group">
                                            <label for="alamat" class="col-form-label">Alamat</label>
                                            <textarea name="alamat" class="form-control">{{ $admin->alamat }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-block btn-success" value="Simpan">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
