@extends('admin.app')

@section('title')
Gallery
@endsection

@section('breadcrumb')

<li><span>Gallery</span></li>

@endsection

@section('content')
<div class="main-content-inner">
     
    <div class="row">
        <div class="col-12 mt-5">
            
            @include('admin.flash')

            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Data Gallery</h4>

                    <a id="addGallery" data-toggle="collapse" href="#accordion41" aria-expanded="true" class="btn btn-info btn-sm">Tambah Gallery</a>
                    <!-- form input -->
                    <div id="accordion4" class="according accordion-s3 gradiant-bg col-md-6 my-form">
                        <div class="card">
                            <div id="accordion41" class="collapse" data-parent="#accordion4" style="">
                                <div class="card-body">
                                    <form action="{{ route('gallery.store') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label for="name" class="col-form-label">Nama Foto</label>
                                            <input class="form-control" type="text" value="{{ old('name') }}" id="name" name="name">
                                        </div>
                                      
                                        <div class="form-group">
                                            <label for="deskripsi" class="col-form-label">Foto</label>
                                            <input type="file" class="form-control" name="foto">
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-block btn-success" value="Simpan">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <div class="inner-content">
                        <div class="row">
                            
                            @foreach($galleries as $row)
                            <div class="col-md-4 mb-4 bordered">
                                <h5 class="mb-3">{{ $row->name }}</h5>
                                
                               
                                <div class="popup-gallery">
                                    <a href="{{ asset('storage/gallery/'. $row->foto ) }}" title="{{ $row->name }}"><img src="{{ asset('storage/gallery/small/'. $row->foto ) }}"></a>
                                </div>
                                <div class="button-image">
                                    <form action="{{ route('gallery.destroy', ['id' => $row->id]) }}" method="post">
                                        <ul class="d-flex justify-content-center">
                                            <li class="mr-3"><a href="{{ route('gallery.edit', ['id' => $row->id ]) }}" class="text-dark"><i class="fa fa-edit"></i> Edit </a></li>
                                            @csrf
                                            @method('delete')
                                            <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="ti-trash"></i> Hapus</button></li>
                                        </ul>
                                    </form>
                                </div>                                                                                 
                                
                            </div>
                            @endforeach
                        </div>

                        <div class="row">
                            <div class="col-md-12">                            
                                @if ($galleries instanceof \Illuminate\Pagination\LengthAwarePaginator )
                                    {{ $galleries->links() }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>

(function($) {

function toggleContent(){
    if($('.collapse').hasClass('show')){
        $('.inner-content').css('display', 'block');
    } else {
        $('.inner-content').css('display', 'none');
    }
}

$('#addGallery').on('click', function(e) {
    e.preventDefault();

    toggleContent();
});

$(document).ready(function() {
	$('.popup-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
			titleSrc: function(item) {
				return item.el.attr('title') + '<small>by LaSari Bali Weeding and Event</small>';
			}
		}
	});
});

})(jQuery)
</script>
@endpush
