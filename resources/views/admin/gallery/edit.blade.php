@extends('admin.app')

@section('title')
Edit Gallery
@endsection

@section('breadcrumb')

<li><a href="{{ route('gallery.index') }}">Gallery</a></li>
<li><span>Edit</span></li>

@endsection

@section('content')
<div class="main-content-inner">
     
    <div class="row">
        <div class="col-6 mt-5">
            
            @include('admin.flash')

            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Edit Gallery</h4>

                    <div id="accordion4" class="according accordion-s3 gradiant-bg col-md-12 my-form">
                        <div class="card">
                            <div id="accordion41" class="collapse show" data-parent="#accordion4" style="">
                                <div class="card-body">
                                    <form action="{{ route('gallery.update', ['id' => $gallery->id]) }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        @method('put')
                                        <div class="form-group">
                                            <label for="name" class="col-form-label">Nama Foto</label>
                                            <input class="form-control" type="text" value="{{ $gallery->name }}" id="name" name="name">
                                        </div>
                                        <div class="form-group">
                                            <img src="{{ asset('storage/gallery/small/'. $gallery->foto ) }}" alt="{{ $gallery->foto }}" width="100%"> <br>
                                            <label for="deskripsi" class="col-form-label">Foto</label>
                                            <input type="file" class="form-control" name="foto">
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-block btn-success" value="Simpan">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

