        <!-- sidebar menu area start -->
        <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo brand">
                    <a href="{{ url('admin') }}">LaSari Bali</a>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <ul class="metismenu" id="menu">
                            <li class="{{ hasPrefix('master') ? 'active':'' }}">
                            <a href="javascript:void(0)" aria-expanded="true"><i class="ti-dashboard"></i><span>Master</span></a>
                                <ul class="collapse">
                                    <li class="{{ hasPrefix('kategori') ? 'active':'' }}"><a href="{{ route('kategori.index') }}">Kategori</a></li>
                                    <li class="{{ hasPrefix('service') ? 'active':'' }}"><a href="{{ route('service.index') }}">Service</a></li>
                                    <li class="{{ hasPrefix('gallery') ? 'active':'' }}"><a href="{{ route('gallery.index') }}">Gallery</a></li>
                                    <li class="{{ hasPrefix('client') ? 'active':'' }}"><a href="{{ route('client.admin.index') }}">Client</a></li>                                    
                                    <li class="{{ hasPrefix('user') ? 'active':'' }}"><a href="{{ route('user.index') }}">Admin</a></li>
                                </ul>
                            </li>
                            <li class="{{ hasPrefix('transaksi') ? 'active':'' }}">
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-money"></i><span>Transaksi</span></a>
                                <ul class="collapse">
                                    <li class="{{ hasPrefix('order') ? 'active':'' }}"><a href="{{ route('order.index') }}">Pesanan</a></li>
                                </ul>
                            </li>
                            <li class="{{ hasPrefix('laporan') ? 'active':'' }}">
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-file"></i><span>Laporan</span></a>
                                <ul class="collapse">
                                    <li class="{{ hasPrefix('pesanan') ? 'active':'' }}"><a href="{{ route('laporan.pesanan') }}">Pesanan</a></li>
                                    <li class="{{ hasPrefix('pembayaran') ? 'active':'' }}"><a href="{{ route('laporan.pembayaran') }}">Pembayaran</a></li>
                                </ul>
                            </li>
                        </ul>
                      
                            
                    
                    </nav>
                </div>
            </div>
        </div>
        <!-- sidebar menu area end -->