@extends('admin.app')

@section('title')
Client
@endsection

@section('breadcrumb')

<li><span>client</span></li>

@endsection

@section('content')
<div class="main-content-inner">
     
    <div class="row">
        <div class="col-12 mt-5">
            
            @include('admin.flash')

            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Data Client</h4>

                    <br>
                    <div class="data-tables inner-content">
                        <table id="dataTable" class="text-center">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th>No</th>
                                    <th>Nama Client</th>
                                    <th>Email</th>
                                    <th>Alamat</th>
                                    <th>No Telp</th>
                                    <th>Button</th>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach($clients as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->nama_client }}</td>
                                        <td>{{ $row->email }}</td>
                                        <td>{{ $row->alamat }}</td>
                                        <td>{{ $row->no_telp }}</td>
                                        <td>
                                            <form action="{{ route('client.admin.destroy', ['id' => $row->id]) }}" method="post">
                                                <ul class="d-flex justify-content-center">                                                   
                                                    @csrf
                                                    @method('delete')
                                                    <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="ti-trash"></i></button></li>
                                                </ul>
                                            </form>
                                        </td>       
                                    </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>

(function($) {


function toggleContent(){
    if($('.collapse').hasClass('show')){
        $('.inner-content').css('display', 'block');
    } else {
        $('.inner-content').css('display', 'none');
    }
}

$('#addCategory').on('click', function(e) {
    e.preventDefault();

    toggleContent();
});


})(jQuery)
</script>
@endpush
