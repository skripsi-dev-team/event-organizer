@extends('admin.app')

@section('title')
Detail Order 
@endsection

@section('breadcrumb')

<li><span>Order</span></li>

@endsection

@section('content')
<div class="main-content-inner">
    <div class="row">
        <div class="col-md-12 mt-5 mb-5">
            @include('client.flash')
            <div class="card">
                <div class="card-body">
                    <h3 class="">Detail Pesanan</h3>
                    <br>
                    <div class="row">
                        <div class="col-md-8 mb-5">
                            <table class="table">
                                <tr>
                                    <td>Pemesan</td>
                                    <td>:</td>
                                    <td>{{ $order->client->nama_client }}</td>
                                </tr>
                                <tr>
                                    <td>Nomor Order</td>
                                    <td>:</td>
                                    <td>{{ $order->nomor_pesanan }}</td>
                                </tr>
                                <tr>
                                    <td>Nama Acara</td>
                                    <td>:</td>
                                    <td>{{ $order->nama_acara }}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Acara</td>
                                    <td>:</td>
                                    <td>{{ $order->tgl_acara->format('d M Y') }}</td>
                                </tr>
                                <tr>
                                    <td>Alamat Acara</td>
                                    <td>:</td>
                                    <td>{{ $order->alamat_acara }}</td>
                                </tr>
                                <tr>
                                    <td>Total Harga</td>
                                    <td>:</td>
                                    <td>{{ 'Rp. '.number_format($order->total_harga) }}</td>
                                </tr>
                                <tr>
                                    <td>Catatan </td>
                                    <td>:</td>
                                    <td>{{ $order->catatan }}</td>
                                </tr>
                            </table>

                            <h4>Data Layanan</h4>
                            <br>
                            <div class="single-table">
                                <div class="table-responsive">
                                    <table class="table text-center">
                                        <thead class="text-uppercase bg-dark">
                                            <tr class="text-white">
                                                <th scope="col">No</th>
                                                <th scope="col">Kode</th>
                                                <th scope="col">Nama</th>
                                                <th scope="col">Kategori</th>
                                                <th scope="col">Harga</th>
                                              
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($order->details as $service)
                                                @if($service->service == null) 
                                                <tr>
                                                    <td colspan="6">Data Service Telah diHapus</td>
                                                </tr>
                                                @else
                                                <tr>
                                                    <th scope="row">{{ $loop->iteration }}</th>
                                                    <td>{{ $service->service->kode_service }}</td>
                                                    <td>{{ $service->service->nama_service }}</td>
                                                    <td>{{ $service->service->category->nama_kategori }}</td>
                                                    <td>{{ 'Rp. '.number_format($service->service->harga) }}</td>
                                                
                                                </tr>
                                                @endif
                                                
                                            @empty
                                            <tr>
                                                <td colspan="6" class="text-center">Client ini tidak memiliki service</td>
                                            </tr>

                                            @endforelse
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <br>

                            <h4>Data Pembayaran</h4>
                            <br>
                            <div class="single-table">
                                <div class="table-responsive">
                                    <table class="table text-center">
                                        <thead class="text-uppercase bg-dark">
                                            <tr class="text-white">
                                                <th scope="col">No</th>
                                                <th scope="col">Tanggal</th>
                                                <th scope="col">Nominal</th>
                                                <th scope="col">Status</th>
                                                <th scope="col">Bukti</th>
                                                <th scope="col">Hapus</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($order->payments as $payment)

                                            <tr>
                                                <th scope="row">{{ $loop->iteration }}</th>
                                                <td>{{ $payment->tgl_pembayaran->format('d M Y') }}</td>
                                                <td>{{ 'Rp. '.number_format($payment->nominal_pembayaran) }}</td>
                                                <td>{{ $payment->getStatusText() }}</td>
                                                <td class="bukti-transfer">
                                                    <a href="{{ asset('storage/bukti_transfer/'. $payment->bukti_transfer ) }}" title="{{ $payment->bukti_transfer }}"><img src="{{ asset('storage/bukti_transfer/'. $payment->bukti_transfer ) }}"></a>
                                                </td>
                                                <td>
                                                    <form action="{{ route('payment.delete', ['payment_id' => $payment->id]) }}" method="post">
                                                        @csrf 
                                                        @method('delete')
                                                        <button type="submit" onclick="return confirm('Yakin hapus layanan?')">
                                                            <i class="ti-trash"></i>
                                                        </button>

                                                    </form>    
                                                </td>
                                            </tr>

                                            @empty
                                            <tr>
                                                <td colspan="6" class="text-center">Client ini belum melakukan pembayaran</td>
                                            </tr>

                                            @endforelse
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">                        
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <h4 class="header-title">Update Status Pesanan</h4>
                                    <form action="{{ route('order.update', $order->id) }}" class="form form-inline" method="post">
                                        @csrf 
                                        @method('put')
                                        <div class="form-group">
                                            <select name="status_pesanan" id="status" class="form-control" style="height: 50px">
                                                @for ($i = 0; $i < 6; $i++)
                                                    <option value="{{ $i }}" {{ ($order->status_pesanan == $i) ? 'selected':'' }}>{{ statusText($i) }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="form-group mt-2">
                                            <button type="submit" class="btn btn-default">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-12">
                                    <h4 class="header-title">Status Pesanan</h4>
                                    <div class="slimScrollDiv">
                                        <div class="timeline-area">
                                            @if($order->status_pesanan == 5)
                                                <div class="timeline-task">
                                                        
                                                    <div class="icon bg-danger">
                                                        <i class="fa fa-times"></i>
                                                        
                                                    </div>
                                                    <div class="tm-title">Pesanan Dibatalkan</div>
                                                </div>
                                            @else

                                                @for ($i = 0; $i < 5; $i++)
                                                    <div class="timeline-task">
                                                        @if ($order->status_pesanan > $i)
                                                            <div class="icon bg-success">
                                                        @elseif($order->status_pesanan == $i)
                                                            <div class="icon bg-warning">
                                                        @else
                                                            <div class="icon bg-secondary">
                                                        @endif
                                                            <i class="fa fa-hand-o-right"></i>
                                                        </div>
                                                        <div class="tm-title">
                                                            <h4>{{ $order->statusText[$i] }}</h4>                                                
                                                        </div>
                                                    </div>
                                                @endfor
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                
                            </div>
                        
                   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
<script>

$(document).ready(function() {
	$('.bukti-transfer').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
			titleSrc: function(item) {
				return item.el.attr('title') + '<small>Bukti Transfer</small>';
			}
		}
	});
});

</script>
@endpush