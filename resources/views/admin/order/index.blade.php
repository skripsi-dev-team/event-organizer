@extends('admin.app')

@section('title')
Data Order 
@endsection

@section('breadcrumb')

<li><span>Order</span></li>

@endsection

@section('content')
<div class="main-content-inner">
     
    <div class="row">
        <div class="col-12 mt-5">
            
            @include('admin.flash')

            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Data Order</h4>

                    <br>
                    <div class="data-tables inner-content">
                        <table id="dataTable" class="text-center">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th>No</th>
                                    <th>Nomor Order</th>
                                    <th>Nama Client</th>
                                    <th>Status</th>
                                    <th>Total Harga</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach($orders as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->nomor_pesanan }}</td>
                                        <td>{{ $row->client->nama_client }}</td>
                                        <td><span class="status-p {{ orderStatusColor($row->status_pesanan) }}">{{ $row->getStatusText() }}</span></td>
                                        <td>{{ 'Rp. '.number_format($row->total_harga) }}</td>
                                        <td>
                                            <a href="{{ route('order.show', $row->id) }}" class="btn btn-success">Detail</a>
                                        </td>       
                                    </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>

(function($) {



})(jQuery)
</script>
@endpush
