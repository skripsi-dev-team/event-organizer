@extends('admin.app')

@section('title')
Service / Layanan
@endsection

@section('breadcrumb')

<li><span>Service</span></li>

@endsection

@section('content')
<div class="main-content-inner">
     
    <div class="row">
        <div class="col-12 mt-5">
            
            @include('admin.flash')

            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Data Service / Layanan</h4>

                    <a data-toggle="collapse" href="#accordion41" aria-expanded="true" class="btn btn-info btn-sm" onclick="toggleContent()">Tambah Service</a>
                    <!-- form input -->
                    <div id="accordion4" class="according accordion-s3 gradiant-bg col-md-6 my-form">
                        <div class="card">
                            <div id="accordion41" class="collapse" data-parent="#accordion4" style="">
                                <div class="card-body">
                                    <form action="{{ route('service.store') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label for="service" class="col-form-label">Nama Service</label>
                                            <input class="form-control" type="text" value="{{ old('nama_service') }}" id="nama_service" name="nama_service" onkeyup="makeSlug()">
                                            <input type="hidden" id="slug" name="slug" value="{{ old('slug') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="kategori_id">Kategori</label>
                                            <select name="kategori_id" class="custom-select">
                                                <option value=""> - Pilih - </option>
                                                @foreach($categories as $kategori)
                                                    <option value="{{ $kategori->id }}">{{ $kategori->nama_kategori }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="harga">Harga</label>
                                            <input type="number" class="form-control" name="harga" value="{{ old('harga') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="deskripsi" class="col-form-label">Deskripsi</label>
                                            <textarea name="deskripsi" id="myeditor"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="thumbnail">Thumbnail</label>
                                            <input type="file" class="form-control" name="thumbnail">                                       
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-block btn-success" value="Simpan">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <div class="data-tables inner-content">
                        <table id="dataTable" class="text-center service-table">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Nama Service</th>
                                    <th>Kategori</th>
                                    <th>Harga</th>                                   
                                    <th>Button</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($services as $row)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $row->kode_service }}</td>
                                    <td>{{ $row->nama_service }}</td>
                                    <td>{{ $row->category->nama_kategori }}</td>
                                    <td>Rp. {{ number_format($row->harga) }}</td>
                                    <td>
                                        <form action="{{ route('service.destroy', ['id' => $row->id]) }}" method="post">
                                        <ul class="d-flex justify-content-center">
                                            <li class="mr-3"><a href="{{ route('service.show', ['id' => $row->id]) }}" class="text-info"><i class="fa fa-eye"></i></a></li>
                                            <li class="mr-3"><a href="{{ route('service.edit', ['id' => $row->id ]) }}" class="text-secondary"><i class="fa fa-edit"></i></a></li>
                                            @csrf
                                            @method('delete')
                                            <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="ti-trash"></i></button></li>
                                        </ul>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>

function makeSlug(){
    var name = $('#nama_service').val();
    var slugName = name.split(' ').join('-').toLowerCase();
    $('#slug').val(slugName);
}

function toggleContent(){
    if($('.collapse').hasClass('show')){
        $('.inner-content').css('display', 'block');
    } else {
        $('.inner-content').css('display', 'none');
    }
}

</script>
@endpush
