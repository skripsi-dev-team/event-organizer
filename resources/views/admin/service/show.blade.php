@extends('admin.app')

@section('title')
Detail Service
@endsection

@section('breadcrumb')

<li><a href="{{ route('service.index') }}">Service</a></li>
<li><span>Show</span></li>

@endsection

@section('content')
<div class="main-content-inner">
     
    <div class="row">
        <div class="col-6 mt-5">
            
            @include('admin.flash')

            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Detail Service</h4>

                    <div id="accordion4" class="according accordion-s3 gradiant-bg col-md-12 my-form">
                        <div class="card">
                            <div id="accordion41" class="collapse show" data-parent="#accordion4" style="">
                                <div class="card-body">
                                    <form action="{{ route('service.update', ['id' => $service->id]) }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        @method('put')
                                        <div class="form-group">
                                            <label for="service" class="col-form-label">Nama Service</label>
                                            <p>{{ $service->nama_service }}</p>
                                        </div>
                                        <div class="form-group">
                                            <label for="kategori_id">Kategori</label>
                                            <p>{{ $service->category->nama_kategori }}</p>
                                        </div>
                                        <div class="form-group">
                                            <label for="harga">Harga</label>
                                            <p>{{ $service->harga }}</p>
                                        </div>
                                        <div class="form-group">
                                            <label for="deskripsi" class="col-form-label">Deskripsi</label>
                                            {!! $service->deskripsi !!}
                                        </div>
                                        <div class="form-group">
                                            <label for="thumbnail">Thumbnail</label>
                                            <br>
                                            <img src="/storage/service_thumbnail/{{ $service->thumbnail }}" alt="{{ $service->thumbnail }}">                                      
                                        </div>

                                        <a href="{{ route('service.index') }}"><i class="fa fa-arrow-left"></i> Kembali</a>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>

function makeSlug(){
    var name = $('#nama_service').val();
    var slugName = name.split(' ').join('-').toLowerCase();
    $('#slug').val(slugName);
}
</script>
@endpush
