@extends('admin.app')

@section('title')
Edit service
@endsection

@section('breadcrumb')

<li><a href="{{ route('service.index') }}">Service</a></li>
<li><span>Edit</span></li>

@endsection

@section('content')
<div class="main-content-inner">
     
    <div class="row">
        <div class="col-6 mt-5">
            
            @include('admin.flash')

            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Edit Service</h4>

                    <div id="accordion4" class="according accordion-s3 gradiant-bg col-md-12 my-form">
                        <div class="card">
                            <div id="accordion41" class="collapse show" data-parent="#accordion4" style="">
                                <div class="card-body">
                                    <form action="{{ route('service.update', ['id' => $service->id]) }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        @method('put')
                                        <div class="form-group">
                                            <label for="service" class="col-form-label">Nama Service</label>
                                            <input class="form-control" type="text" value="{{ $service->nama_service }}" id="nama_service" name="nama_service" onkeyup="makeSlug()">
                                            <input type="hidden" id="slug" name="slug" value="{{ $service->slug}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="kategori_id">Kategori</label>
                                            <select name="kategori_id" class="custom-select">
                                                <option value=""> - Pilih - </option>
                                                @foreach($categories as $kategori)
                                                    <option value="{{ $kategori->id }}" {{ ($kategori->id == $service->category_id) ? 'selected' : '' }} >{{ $kategori->nama_kategori }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="harga">Harga</label>
                                            <input type="number" class="form-control" name="harga" value="{{ $service->harga }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="deskripsi" class="col-form-label">Deskripsi</label>
                                            <textarea name="deskripsi" id="myeditor">{{ $service->deskripsi }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="thumbnail">Thumbnail</label>
                                            <input type="file" class="form-control" name="thumbnail">                                       
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-block btn-success" value="Simpan">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>

function makeSlug(){
    var name = $('#nama_service').val();
    var slugName = name.split(' ').join('-').toLowerCase();
    $('#slug').val(slugName);
}
</script>
@endpush
