@extends('admin.app')

@section('title')
Laporan Pembayaran
@endsection

@section('breadcrumb')

<li><span>Laporan Pembayaran</span></li>

@endsection

@section('content')
<div class="main-content-inner">
     
    <div class="row">
        <div class="col-12 mt-5">
            @include('admin.flash')
            <div class="card">
                <div class="card-body">
                    <h5 class="header-title">Tampilkan Laporan Pembayaran</h5>
                    <form action="{{ route('laporan.pembayaran') }}" method="get">
                        <div class="row">
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Dari Tanggal</label>
                                    <input type="text" class="form-control datepicker" name="start_date" value="{{ (isset($_GET['start_date'])) ? $_GET['start_date'] : '' }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Sampai Tanggal</label>
                                    <input type="text" class="form-control datepicker" name="end_date" value="{{ (isset($_GET['end_date'])) ? $_GET['end_date'] : '' }}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input type="submit" value="CARI" name="pembayaran" class="btn btn-secondary float-right">
                            </div>
                        
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
     @if($payments != null)
    <div class="row">
        <div class="col-md-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <p><a href='{{ url("admin/laporan/pembayaran/print?start_date=$_GET[start_date]&end_date=$_GET[end_date]&pembayaran=$_GET[pembayaran]") }}'>Cetak PDF</a></p>
                    <br>
                    <table class="table">
                        <thead class="bg-light text-capitalize">
                            <tr>
                                <th>No</th>
                                <th>Nomor Order</th>
                                <th>Tanggal Pembayaran</th>
                                <th>Nominal</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($payments as $payment)

                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td><a href="{{ route('order.show', $payment->order->id) }}">{{ $payment->order->nomor_pesanan }}</a></td>
                                <td>{{ $payment->tgl_pembayaran->format('d M Y') }}</td>
                                <td>{{ 'Rp. '.number_format($payment->nominal_pembayaran) }}</td>
                                <td>{{ $payment->getStatusText() }}</td>                                
                            </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @else
    <br><br><br><br><br><br>
    @endif
@endsection

@push('scripts')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
<script src="{{ asset('templates/assets/js/bootstrap-datepicker.min.js') }}"></script>
<script>

$('.datepicker').datepicker({
    format: 'yyyy-mm-dd'
});

</script>
@endpush