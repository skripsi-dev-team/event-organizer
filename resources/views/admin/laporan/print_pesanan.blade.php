<style>

body {
    font-family: "Arial";
}

.body-print {
    max-width: 1280px;
    margin:auto;
}

.header-print {
    text-align:center;
}

.content-print {
    margin-top: 30px;
}

.title {
    font-weight:bold;
    font-size:18px;
}

table {
  width: 100%;
  border-collapse: collapse;
}

th {
  height: 30px;
  text-align:center;
}

table, th, td {
  border: 1px solid black;
  padding:8px;
}

table tr td:first-child {
    text-align:center;
}


</style>
<div class="body-print">
    <div class="header-print">
        <h2>LaSari Bali Wedding & Event</h2>
        <p>Jalan Karangsari No. 10 IX, Padangsambian Kaja, Denpasar Barat, Bali 80116</p>
        <p>Telp: 081-xxx-xxx-xxx | IG: @loremipsum | Email: xxxx@gmail.com</p>
    </div>
    <hr>
    <div class="content-print">
        <p class="title">Laporan Pesanan Periode: {{ $_GET['start_date'] }} s.d {{ $_GET['end_date'] }}</p>
        <table class="table-print">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nomor Order</th>
                    <th>Nama Acara</th>
                    <th>Tanggal Pesan</th>
                    <th>Nama Client</th>
                    <th>Status</th>
                    <th>Total Harga</th>
                </tr>
            </thead>
            <tbody>
                @foreach($orders as $row)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $row->nomor_pesanan }}</td>
                        <td>{{ $row->nama_acara }}</td>
                        <td>{{ explode(' ', $row->tgl_pesanan)[0] }}</td>
                        <td>{{ $row->client->nama_client }}</td>
                        <td>{{ $row->getStatusText() }}</td>
                        <td>{{ 'Rp. '.number_format($row->total_harga) }}</td>
                            
                    </tr>
                @endforeach
            </tbody>
        </table>
        <br>
        <br>
        <br>
        Denpasar, {{ date('d M Y') }}
        <br><br><br><br>
        ({{ ucwords(Auth::guard('web')->user()->name) }})
    </div>
</div>