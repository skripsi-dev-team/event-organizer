<style>

body {
    font-family: "Arial";
}

.body-print {
    max-width: 1280px;
    margin:auto;
}

.header-print {
    text-align:center;
}

.content-print {
    margin-top: 30px;
}

.title {
    font-weight:bold;
    font-size:18px;
}

table {
  width: 100%;
  border-collapse: collapse;
}

th {
  height: 30px;
  text-align:center;
}

table, th, td {
  border: 1px solid black;
  padding:8px;
}

table tr td:first-child {
    text-align:center;
}


</style>
<div class="body-print">
    <div class="header-print">
        <h2>LaSari Bali Wedding & Event</h2>
        <p>Jalan Karangsari No. 10 IX, Padangsambian Kaja, Denpasar Barat, Bali 80116</p>
        <p>Telp: 081-xxx-xxx-xxx | IG: @loremipsum | Email: xxxx@gmail.com</p>
    </div>
    <hr>
    <div class="content-print">
        <p class="title">Laporan Pesanan Periode: {{ $_GET['start_date'] }} s.d {{ $_GET['end_date'] }}</p>
        <table class="table">
            <thead class="bg-light text-capitalize">
                <tr>
                    <th>No</th>
                    <th>Nomor Order</th>
                    <th>Tanggal Pembayaran</th>
                    <th>Nominal</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($payments as $payment)

                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $payment->order->nomor_pesanan }}</td>
                    <td>{{ $payment->tgl_pembayaran->format('d M Y') }}</td>
                    <td>{{ 'Rp. '.number_format($payment->nominal_pembayaran) }}</td>
                    <td>{{ $payment->getStatusText() }}</td>                                
                </tr>

                @endforeach
            </tbody>
        </table>
        <br>
        <br>
        <br>
        Denpasar, {{ date('d M Y') }}
        <br><br><br><br>
        (.....................................)
    </div>
</div>