@extends('admin.app')

@section('title')
Edit Kategori
@endsection

@section('breadcrumb')

<li><a href="{{ route('kategori.index') }}">Kategori</a></li>
<li><span>Edit</span></li>

@endsection

@section('content')
<div class="main-content-inner">
     
    <div class="row">
        <div class="col-6 mt-5">
            
            @include('admin.flash')

            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Edit Kategori</h4>

                    <div id="accordion4" class="according accordion-s3 gradiant-bg col-md-12 my-form">
                        <div class="card">
                            <div id="accordion41" class="collapse show" data-parent="#accordion4" style="">
                                <div class="card-body">
                                    <form action="{{ route('kategori.update', ['id' => $category->id]) }}" method="post">
                                        @csrf
                                        @method('put')
                                        <div class="form-group">
                                            <label for="nama_kategori" class="col-form-label">Nama Kategori</label>
                                            <input class="form-control" type="text" value="{{ $category->nama_kategori }}" id="nama_kategori" name="nama_kategori" onkeyup="makeSlug()">
                                            <input type="hidden" id="slug" name="slug" value="{{ $category->slug }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="deskripsi" class="col-form-label">Deskripsi</label>
                                            <textarea name="deskripsi" id="myeditor">{{ $category->deskripsi }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-block btn-success" value="Simpan">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>

function makeSlug(){
    var name = $('#nama_kategori').val();
    var slugName = name.split(' ').join('-').toLowerCase();
    $('#slug').val(slugName);
}
</script>
@endpush
