@extends('admin.app')

@section('title')
Kategori
@endsection

@section('breadcrumb')

<li><span>Kategori</span></li>

@endsection

@section('content')
<div class="main-content-inner">
     
    <div class="row">
        <div class="col-12 mt-5">
            
            @include('admin.flash')

            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Data Kategori</h4>

                    <a id="addCategory" data-toggle="collapse" href="#accordion41" aria-expanded="true" class="btn btn-info btn-sm">Tambah Kategori</a>
                    <!-- form input -->
                    <div id="accordion4" class="according accordion-s3 gradiant-bg col-md-6 my-form">
                        <div class="card">
                            <div id="accordion41" class="collapse" data-parent="#accordion4" style="">
                                <div class="card-body">
                                    <form action="{{ route('kategori.store') }}" method="post">
                                        @csrf
                                        <div class="form-group">
                                            <label for="nama_kategori" class="col-form-label">Nama Kategori</label>
                                            <input class="form-control" type="text" value="{{ old('nama_kategori') }}" id="nama_kategori" name="nama_kategori">
                                        </div>
                                        <div class="form-group">
                                            <label for="slug">Slug</label>
                                            <input type="text" id="slug" class="form-control" name="slug" value="{{ old('slug') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="deskripsi" class="col-form-label">Deskripsi</label>
                                            <textarea name="deskripsi" id="myeditor"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-block btn-success" value="Simpan">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <div class="data-tables inner-content">
                        <table id="dataTable" class="text-center">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th>No</th>
                                    <th>Nama Kategori</th>
                                    <th>Deskripsi</th>
                                    <th>Button</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $row)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $row->nama_kategori }}</td>
                                    <td>{!! $row->deskripsi !!}</td>
                                    <td>
                                        <form action="{{ route('kategori.destroy', ['id' => $row->id]) }}" method="post">
                                        <ul class="d-flex justify-content-center">
                                            <li class="mr-3"><a href="{{ route('kategori.edit', ['id' => $row->id ]) }}" class="text-secondary"><i class="fa fa-edit"></i></a></li>
                                            @csrf
                                            @method('delete')
                                            <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="ti-trash"></i></button></li>
                                        </ul>
                                        </form>
                                    </td>                                  
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>

(function($) {

function makeSlug(input, place){
    var name = $('#nama_kategori').val();
    place.val(name.replace(/ /g, '-').replace(/[!@#$%^&*()_=+?.,]/g, '').toLowerCase());
}

function toggleContent(){
    if($('.collapse').hasClass('show')){
        $('.inner-content').css('display', 'block');
    } else {
        $('.inner-content').css('display', 'none');
    }
}

$('#addCategory').on('click', function(e) {
    e.preventDefault();

    toggleContent();
});

$('#nama_kategori').on('keyup', function(e) {
    if (e.keyCode == 13) {
        return;
    }

    makeSlug($(this), $('#slug'));
});

})(jQuery)
</script>
@endpush
