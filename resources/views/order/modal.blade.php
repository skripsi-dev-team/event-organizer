<div class="modal fade" id="orderModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('client.order', Auth::guard('client')->check() ? Auth::guard('client')->user()->id:0) }}" method="post">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Mohon mengisi Form Dibawah Sebelum Membuat Pesanan</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="service_id">
                    <div class="form-group">
                        <label for="namaAcara">Nama Acara</label>
                        <input type="text" name="nama_acara" id="namaAcara" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="tanggalAcara">Tanggal Acara</label>
                        <input type="text" name="tgl_acara" id="tanggalAcara" class="form-control datepicker">
                    </div>
                    <div class="form-group">
                        <label for="alamatAcara">Alamat Acara</label>
                        <textarea name="alamat_acara" id="alamatAcara" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="catatan">Catatan</label>
                        <textarea name="catatan" id="catatan" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary" onclick="return confirm('Apakah anda yakin akan melakukan pemesanan?')" >Buat Pesanan</button>
                </div>
            </form>
        </div>
    </div>
</div>