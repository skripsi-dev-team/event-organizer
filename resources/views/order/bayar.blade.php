@extends('client.app')

@section('title')
Bayar Pesanan {{ $order->nomor_pesanan }}
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-5 mb-5">
            @include('client.flash')
            <div class="card">
                <div class="card-body">
                    <h3 class="">Form Pembayaran</h3>
                    <br>
                    <div class="row">
                        <div class="col-md-8 mb-5">
                        <form action="{{ route('client.pembayaran') }}" method="post" enctype="multipart/form-data">
                            @csrf                                                
                            <input type="hidden" name="order_id" value="{{ $order->id }}">
                            <div class="form-group">
                                <label for="nomor_pesanan">Nomor Pesanan</label>
                                <input type="text" name="nomor_pesanan" id="nomor_pesanan" class="form-control" value="{{ $order->nomor_pesanan }}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="nama_acara">Nama Acara</label>
                                <input type="text" name="nama_acara" id="nama_acara" class="form-control" value="{{ $order->nama_acara }}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="tgl_pembayaran">Tanggal Pembayaran</label>
                                <input type="text" name="tgl_pembayaran" id="tgl_pembayaran" class="form-control" value="{{ now() }}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="nominal_pembayaran">Nominal Pembayaran (pastikan jumlah transfer sesuai)</label>
                                <input type="text" id="nominal_pembayaran" class="form-control" value="Rp. {{ number_format($order->total_harga) }}" readonly>
                                <input type="hidden" name="nominal_pembayaran" value="{{ $order->total_harga }}">
                            </div>                                
                            <div class="form-group">
                                <label for="catatan">Bukti Transfer</label>
                                <input type="file" name="bukti_transfer" class="form-control" required>
                            </div>  
                            <div class="form-group text-right">
                                <input type="submit" class="btn btn-lg btn-success" value="Bayar Sekarang">
                            </div>                        
                        </form> 

                        
                        </div>
                        <div class="col-md-4">                        
                            <div class="data-rekening">
                                <h4>Transfer via Bank</h4>
                                <img src="{{ asset('img/logo-bni.png') }}" alt="bank" class="img-fluid">
                                <p>Nomor Rekening: </p>
                                <p>xxx-xxx-xx-xxx</p>
                                <p>A/n <b>Lorem ipsum dolor sit amet</b></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection