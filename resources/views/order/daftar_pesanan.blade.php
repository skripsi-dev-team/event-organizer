@extends('client.app')

@section('title')
Pesanan Saya
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-5 mb-5">
            @include('client.flash')
            <div class="card">
                <div class="card-body">
                    <h3 class="">Daftar Pesanan Anda</h3>
                    <br>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nomor Pesanan</th>
                                <th>Nama Acara</th>
                                <th>Tanggal Acara</th>
                                <th>Alamat Acara</th>
                                <th>Total Harga</th>
                                <th>Status Pesanan</th>
                                <th>Button</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $order->nomor_pesanan }}</td>
                                    <td>{{ $order->nama_acara }}</td>
                                    <td>{{ $order->tgl_acara->format('d M Y') }}</td>
                                    <td>{{ $order->alamat_acara }}</td>
                                    <td>{{ 'Rp. '.number_format($order->total_harga) }}</td>
                                    <td><span class="status-p {{ orderStatusColor($order->status_pesanan) }}">{{ $order->getStatusText() }}</span></td>
                                    <td>
                                    <ul class="d-flex justify-content-center">
                                            <li class="mr-3"><a href="{{ route('client.pesanan.bayar', ['order'=> strtolower($order->nomor_pesanan)] )  }}" class="btn btn-sm btn-success {{ ($order->status_pesanan != 0) ? 'disabled' : '' }}">Bayar</a></li>                                         
                                            <li><a href="{{ route('client.pesanan.detail', ['order' => strtolower($order->nomor_pesanan)]) }}" class="btn btn-sm btn-outline-primary">Detail</a></li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection