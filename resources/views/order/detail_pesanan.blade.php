@extends('client.app')

@section('title')
Detail Pesanan {{ $order->nomor_pesanan }}
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-5 mb-5">
            @include('client.flash')
            <div class="card">
                <div class="card-body">
                    <h3 class="">Detail Pesanan</h3>
                    <br>
                    <div class="row">
                        <div class="col-md-8 mb-5">
                            <table class="table pesanan-table">
                                <tr>
                                    <td>Nomor Order</td>
                                    <td>:</td>
                                    <td>{{ $order->nomor_pesanan }}</td>
                                </tr>
                                <tr>
                                    <td>Nama Acara</td>
                                    <td>:</td>
                                    <td>{{ $order->nama_acara }}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Acara</td>
                                    <td>:</td>
                                    <td>{{ $order->tgl_acara->format('d M Y') }}</td>
                                </tr>
                                <tr>
                                    <td>Alamat Acara</td>
                                    <td>:</td>
                                    <td>{{ $order->alamat_acara }}</td>
                                </tr>
                                <tr>
                                    <td>Total Harga</td>
                                    <td>:</td>
                                    <td>{{ 'Rp. '.number_format($order->total_harga) }}</td>
                                </tr>
                                <tr>
                                    <td>Catatan </td>
                                    <td>:</td>
                                    <td>{{ $order->catatan }}</td>
                                </tr>
                            </table>

                            <h4>Data Layanan</h4>
                            <br>
                            <div class="single-table">
                                <div class="table-responsive">
                                    <table class="table text-center">
                                        <thead class="text-uppercase bg-dark">
                                            <tr class="text-white">
                                                <th scope="col">No</th>
                                                <th scope="col">Kode</th>
                                                <th scope="col">Nama</th>
                                                <th scope="col">Kategori</th>
                                                <th scope="col">Harga</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($order->details as $service)
                                                @if($service->service == null) 
                                                <tr>
                                                    <td colspan="6">Data Service Telah diHapus</td>
                                                </tr>
                                                @else

                                                <tr>
                                                    <th scope="row">{{ $loop->iteration }}</th>
                                                    <td>{{ $service->service->kode_service }}</td>
                                                    <td>{{ $service->service->nama_service }}</td>
                                                    <td>{{ $service->service->category->nama_kategori }}</td>
                                                    <td>{{ 'Rp. '.number_format($service->service->harga) }}</td>
                                                    <td>
                                                        <form action="{{ route('client.service.delete', ['order_id' => $order->id, 'service_id' => $service->service->id]) }}" method="post">
                                                            @csrf 
                                                            @method('delete')
                                                            <button type="submit" onclick="return confirm('Yakin hapus layanan?')">
                                                                <i class="ti-trash"></i>
                                                            </button>

                                                        </form>    
                                                    
                                                    </td>
                                                </tr>
                                                @endif

                                            @endforeach
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">                                                
                                <h4 class="header-title">Status Pesanan</h4>
                                <div class="slimScrollDiv">
                                    <div class="timeline-area">
                                        @if($order->status_pesanan == 5)
                                                <div class="timeline-task">                                                        
                                                    <div class="icon bg-danger">
                                                        <i class="fa fa-times"></i>                                                        
                                                    </div>
                                                    <div class="tm-title">Pesanan Dibatalkan</div>
                                                </div>
                                            @else

                                                @for ($i = 0; $i < 5; $i++)
                                                    <div class="timeline-task">
                                                        @if ($order->status_pesanan > $i)
                                                            <div class="icon bg-success">
                                                        @elseif($order->status_pesanan == $i)
                                                            <div class="icon bg-warning">
                                                        @else
                                                            <div class="icon bg-secondary">
                                                        @endif
                                                            <i class="fa fa-hand-o-right"></i>
                                                        </div>
                                                        <div class="tm-title">
                                                            <h4>{{ $order->statusText[$i] }}</h4>                                                
                                                        </div>
                                                    </div>
                                                @endfor
                                        @endif
                                    </div>
                                </div>
                        
                   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection