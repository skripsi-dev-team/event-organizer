<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Service::class, function (Faker $faker) {
    $name = $faker->sentence(2);
    return [
        'nama_service' => $name,
        'kode_service' => $name,
        'slug' => str_replace(' ', '-', $name),
        'harga' => $faker->numberBetween(100000, 5000000),
        'thumbnail' => '',  
        'deskripsi' => $faker->paragraph,
        'category_id' => function() {
            return App\Category::all()->random()->id;
        },
    ];
});
