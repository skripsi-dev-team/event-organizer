<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Payment::class, function (Faker $faker) {
    $order = App\Orders::all()->random()->id;
    $total = App\Orders::find($order)->total_harga;
    $tgl_bayar = App\Orders::find($order)->tgl_pesanan;
    return [
        'order_id' => $order,
        'tgl_pembayaran' => $tgl_bayar,
        'nominal_pembayaran' => $total,
        'status_pembayaran' => 1,
        'bukti_transfer' => 'seeder'
    ];
});
