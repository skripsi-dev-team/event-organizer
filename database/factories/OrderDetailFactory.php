<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\OrderDetail::class, function (Faker $faker) {
    $tgl_pesan = '2019-02-04 00:00:00';
    return [
        'order_id' => function(){
            return App\Orders::all()->random()->id;
        },
        'service_id' => function(){
            return App\Service::all()->random()->id;
        },
        
    ];
});
