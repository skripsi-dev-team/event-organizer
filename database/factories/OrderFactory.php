<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Orders::class, function (Faker $faker) {
    $tgl_pesan = '2019-02-04 00:00:00';
    return [
        'client_id' => function(){
            return App\Client::all()->random()->id;
        },
        'nomor_pesanan' => generateOrderNumber(),
        'tgl_pesanan' => $tgl_pesan,
        'nama_acara' => $faker->word,
        'tgl_acara' => '2019-02-07 00:00:00',
        'alamat_acara' => $faker->address,
        'total_harga' => $faker->numberBetween(100000, 5000000),
        'catatan' => $faker->sentence(3),
        'status_pesanan' => 4
    ];
});
