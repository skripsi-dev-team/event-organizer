<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Category::class, function (Faker $faker) {
    $name = $faker->word;
    return [
        'nama_kategori' => $name,
        'slug' => strtolower($name),
        'deskripsi' => $faker->sentence(5)
    ];
});
