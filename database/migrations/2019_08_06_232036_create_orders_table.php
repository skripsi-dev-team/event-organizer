<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_id')->unsigned();
            $table->string('nomor_pesanan')->nullable();
            $table->dateTime('tgl_pesanan');
            $table->string('nama_acara');
            $table->dateTime('tgl_acara');
            $table->text('alamat_acara');
            $table->integer('total_harga')->unsigned();
            $table->text('catatan')->nullable();
            $table->tinyInteger('status_pesanan');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
