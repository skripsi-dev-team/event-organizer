<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'nama_kategori',
        'deskripsi',
        'slug'
    ];

    public function service(){
        return $this->hasMany(Service::class);
    }
}
