<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PesananBatalNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    private $order;
    private $client;

    public function __construct($client, $order)
    {
        $this->client = $client;
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->from(env('MAIL_FROM_ADDRESS', 'noreply@kodinggen.com'))
                    ->subject('Pesanan Dibatalkan')
                    ->greeting('Hai, ' . $this->client->nama_client)
                    ->line('Status pesanan anda dengan nomor ' . $this->order->nomor_pesanan . ' kami nyatakan batal.')
                    ->line('Dikarenakan anda tidak melakukan proses pembayaran. Jika anda ingin memesan lagi, silahkan kunjungi website kami dan lakukan pemesanan.')
                    ->action('Lihat status pesanan', url('pesanan-saya/'.$this->order->nomor_pesanan.'/detail'))                  
                    ->line('Terima kasih!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
