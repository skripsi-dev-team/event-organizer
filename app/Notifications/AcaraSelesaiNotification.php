<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AcaraSelesaiNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $client;
    private $order;

    public function __construct($client, $order)
    {
        $this->client = $client;
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)   
                    ->from(env('MAIL_FROM_ADDRESS', 'noreply@kodinggen.com'))                
                    ->subject('Acara Telah Selesai')
                    ->greeting('Hai, ' . $this->client->nama_client)
                    ->line('Acara anda dengan nomor order ' . $this->order->nomor_pesanan . ' telah selesai. Semoga sudah sesuai dengan yang anda harapkan')
                    ->line('Terima kasih telah mempercayakan segala kebutuhan event anda kepada kami. Mohon maaf apabila ada kesalahan dari kami.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
