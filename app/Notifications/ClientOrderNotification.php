<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ClientOrderNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $client;

    private $order;

    public function __construct($client, $order)
    {
        $this->client = $client;
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Order di Diterima')
                    ->from(env('DEFAULT_ADMIN_EMAIL', 'noreply@kodinggen.com'))
                    ->greeting('Haii '.$this->client->nama_client)
                    ->line('Order kamu telah berhasil dibuat dengan nomor order #'.$this->order->nomor_pesanan)
                    ->line('Segera selesaikan pembayaran agar order kamu dapat diproses dengan segera.')
                    ->line('Ketika kamu belum menyelesaikan pembayaran, kamu masih dapat menambah order lain dan total order akan disesuaikan.')
                    ->action('Bayar Sekarang', url('pesanan-saya/'.$this->order->nomor_pesanan.'/bayar'))
                    ->line('Terimakasih telah menggunakan Lasari Wedding Event!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
