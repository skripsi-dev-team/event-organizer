<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class KonfirmasiPembayaranNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $client;
    private $order;
    public function __construct($client, $order)
    {
        $this->client = $client;
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->from(env('MAIL_FROM_ADDRESS', 'noreply@kodinggen.com'))
                    ->subject('Pembayaran Telah Dikonfirmasi')
                    ->greeting('Hai, ' . $this->client->nama_client)
                    ->line('Pembayaran pesanan anda dengan nomor ' . $this->order->nomor_pesanan . ' telah kami konfirmasi.')
                    ->line('Kami akan segera melakukan proses persiapan untuk acara anda.')   
                    ->action('Lihat status pesanan', url('pesanan-saya/'.$this->order->nomor_pesanan.'/detail'))                
                    ->line('Terima kasih!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
