<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Client extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'nama_client', 'email', 'password', 'remember_token', 'no_telp', 'alamat'
    ];

    protected $hidden = ['password'];

    public function orders()
    {
        return $this->hasMany(Orders::class);
    }

    public function unpaidOrder()
    {
        return $this->orders()->where('status_pesanan', 0)->first();
    }
}
