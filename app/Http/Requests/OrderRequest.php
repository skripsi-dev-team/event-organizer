<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function all($data = null)
    {
        $formData = parent::all();
        $formData['nomor_pesanan'] = generateOrderNumber();
        $formData['tgl_pesanan'] = now()->format('Y-m-d');
        $formData['status_pesanan'] = 0;

        return $formData;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service_id' => 'required|integer',
            'tgl_acara' => 'required',
            'alamat_acara' => 'required',        
        ];
    }
}
