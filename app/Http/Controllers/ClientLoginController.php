<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class ClientLoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/';

    public function __construct()
    {
        return $this->middleware('guest:client')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.client.login');
    }

    public function guard()
    {
        return Auth::guard('client');
    }
}
