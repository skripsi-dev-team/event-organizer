<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Orders;
use App\Payment;
use App\User;
use App\Notifications\AdminPaymentNotification;

class PaymentController extends Controller
{
    public function formBayar($no_order)
    {
        $client = Auth::guard('client')->user();
        $order = Orders::where('client_id', $client->id)
                        ->where('nomor_pesanan', $no_order)
                        ->first();
        if($order == null){
            return redirect()->back()->with('error', 'Data order tidak ada.');
        }

        return view('order.bayar')->with('order', $order);
    }

    public function bayar(Request $request)
    {
        //insert ke table payment
        $this->validate($request, [
            'bukti_transfer' => 'required'
        ]);
        
        $request->file('bukti_transfer')->store('bukti_transfer');

        Payment::create([
            'order_id' => $request->order_id,
            'tgl_pembayaran' => $request->tgl_pembayaran,
            'nominal_pembayaran' => $request->nominal_pembayaran,
            'bukti_transfer' => $request->bukti_transfer->hashName(),
            'status_pembayaran' => 1
        ]);

        //ubah status table order
        $order = Orders::find($request->order_id);
        $order->update([
            'status_pesanan' => 1
        ]);

        //notify admin
        $admin = User::where('email', env('DEFAULT_ADMIN_EMAIL', ''))->first();
        $admin->notify(new AdminPaymentNotification($order));

        return redirect()->route('client.pesanan-saya')->with('success', 'Berhasil melakukan pembayaran pesanan '.$request->nomor_pesanan);
    }

    public function delete($id){
        Payment::find($id)->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus data pembayaran');
    }
}
