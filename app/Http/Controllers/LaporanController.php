<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orders;
use App\Payment;
use PDF;
class LaporanController extends Controller
{
    public function pesanan(Request $request){
        $data['orders'] = null;
        if(isset($_GET['pesanan'])){
            if($request->start_date > $request->end_date){
                return redirect()->back()->with('error', 'Tanggal tidak valid!');
            }
            $data['orders'] = Orders::where([
                ['tgl_pesanan', '>=', $request->start_date],
                ['tgl_pesanan', '<=', $request->end_date]
            ])->orderBy('tgl_pesanan', 'asc')->get();
        }
        return view('admin/laporan/pesanan', $data);
    }

    public function printPesanan(Request $request){
        $data['orders'] = Orders::where([
            ['tgl_pesanan', '>=', $request->start_date],
            ['tgl_pesanan', '<=', $request->end_date]
        ])->orderBy('tgl_pesanan', 'asc')->get();

        $pdf = PDF::loadView('admin/laporan/print_pesanan', $data);
        return $pdf->download('laporan_pesanan.pdf');
    }

    public function pembayaran(Request $request){
        $data['payments'] = null;
        if(isset($_GET['pembayaran'])){
            if($request->start_date > $request->end_date){
                return redirect()->back()->with('error', 'Tanggal tidak valid!');
            }
            $data['payments'] = Payment::where([
                ['tgl_pembayaran', '>=', $request->start_date],
                ['tgl_pembayaran', '<=', $request->end_date]
            ])->orderBy('tgl_pembayaran', 'asc')->get();
        }
        return view('admin/laporan/pembayaran', $data);
    }

    public function printPembayaran(Request $request){
        $data['payments'] = Payment::where([
            ['tgl_pembayaran', '>=', $request->start_date],
            ['tgl_pembayaran', '<=', $request->end_date]
        ])->orderBy('tgl_pembayaran', 'asc')->get();

        $pdf = PDF::loadView('admin/laporan/print_pembayaran', $data);
        return $pdf->download('laporan_pembayaran.pdf');
    }
}
