<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.gallery.index')->with('galleries', Gallery::paginate(6));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'foto' => 'required'
        ]);

        $store_image = $request->file('foto')->store('gallery');
        if($store_image) {
            $file_name = $request->foto->hashName();
            resizeImageGallery($file_name);
        }

        Gallery::create([
            'name' => $request->name,
            'foto' => $file_name
        ]);

        return redirect()->back()->with('success', 'Berhasil menambahkan data gallery');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.gallery.edit')->with('gallery', Gallery::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $gallery = Gallery::findOrFail($id);
        $file_name = $gallery->foto;

        if($request->hasFile('foto') == true){
            unlink('storage/gallery/'.$file_name);
            unlink('storage/gallery/small/'.$file_name);
            $request->file('foto')->store('gallery');
            $file_name = $request->foto->hashName();
            resizeImageGallery($file_name);
        }

        $gallery->update([
            'name' => $request->name,
            'foto' => $file_name
        ]);

        return redirect()->route('gallery.index')->with('success', 'Berhasil mengedit data gallery');




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = Gallery::find($id);
        unlink('storage/gallery/'.$gallery->foto);
        unlink('storage/gallery/small/'.$gallery->foto);
        $gallery->delete();

        return redirect()->back()->with('success', 'Berhasil menghapus data gallery');
    }
}
