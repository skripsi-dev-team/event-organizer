<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Category;
use PHPUnit\Framework\Exception;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.service.index', ['services' => Service::all(), 'categories' => Category::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_service' => 'required',
            'kategori_id' => 'required',
            'slug' => 'required',
            'harga' => 'required|numeric'
        ]);

        $file_name = "";

        if($request->has('thumbnail')){
            $store_image = $request->file('thumbnail')->store('service_thumbnail');
            if($store_image){
                $file_name = $request->thumbnail->hashName();
                try {
                    resizeImage($file_name);
                } catch(Exception $e){
                    dd($e->getMessage());
                }
            }
        }

        Service::create([
            'nama_service' => $request->nama_service,
            'kode_service' => $request->nama_service,
            'slug' => $request->slug,
            'category_id' => $request->kategori_id,
            'harga' => $request->harga,
            'deskripsi' => $request->deskripsi,
            'thumbnail' => $file_name
        ]);

        return redirect()->back()->with('success', 'Berhasil menambah data service');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.service.show')->with('service', Service::find($id))->with('categories', Category::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.service.edit')->with('service', Service::find($id))->with('categories', Category::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_service' => 'required',
            'kategori_id' => 'required',
            'slug' => 'required',
            'harga' => 'required|numeric'
        ]);

         $service = Service::find($id);
         $file_name = $service->thumbnail;

         if($request->hasFile('thumbnail') == true){
            if(is_file('storage/service_thumbnail/'.$file_name) && is_file('storage/service_thumbnail/small/'.$file_name)){
                unlink('storage/service_thumbnail/'.$file_name);
                unlink('storage/service_thumbnail/small/'.$file_name);
            } 

            $request->file('thumbnail')->store('service_thumbnail');
            $file_name = $request->thumbnail->hashName();
            try{
                resizeImage($file_name);
            } catch (Exception $e) {
                dd($e->getMessage());
            }
             
            
         }

         $service->update([
            'nama_service' => $request->nama_service,
            'kode_service' => $request->nama_service,
            'slug' => $request->slug,
            'kategori_id' => $request->kategori_id,
            'harga' => $request->harga,
            'deskripsi' => $request->deskripsi,
            'thumbnail' => $file_name
         ]);

         return redirect()->route('service.index')->with('success', 'Berhasil mengedit data service');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::find($id);
        $file_name = $service->thumbnail;
        if(is_file('storage/service_thumbnail/'.$file_name) && is_file('storage/service_thumbnail/small/'.$file_name)){
            unlink('storage/service_thumbnail/'.$file_name);
            unlink('storage/service_thumbnail/small/'.$file_name);
        } 
        
        $service->delete();

        return redirect()->back()->with('success', 'Berhasil menghapus data service'); 
    }
}
