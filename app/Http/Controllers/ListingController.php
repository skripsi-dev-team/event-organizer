<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Service;
use App\Gallery;

class ListingController extends Controller
{
    public function indexService(){
        $data['categories'] = Category::has('service')->get();
        return view('client.listing.all', $data);
    }

    public function showService($slug){
        $data['category'] = Category::where('slug', $slug)->first();
        if(!$data['category']){
            return redirect()->back()->with('error', 'Data belum tersedia!');    
        }
        
        $data['services'] = Service::where('category_id', $data['category']->id)->paginate(9);
        
        return view('client.listing.service', $data);
    }

    public function detailService($slug_kategori, $slug_service){
        $data['service'] = Service::where('slug', $slug_service)->first();
        if(!$data['service']){
            return redirect()->back()->with('error', 'Data belum tersedia!');    
        }
        return view('client.listing.detail', $data);
    }

    public function gallery(){
        $data['galleries'] = Gallery::paginate(12);
        return view('client.listing.gallery', $data);
    }
}
