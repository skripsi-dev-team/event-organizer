<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Validator;
use Auth;

class ClientRegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/client/login';

    public function __construct()
    {
        $this->middleware('guest:client');
    }

    public function showRegisterForm()
    {
        return view('auth.client.register');
    }

    public function register(Request $request)
    {
        // dd($request->all());
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return redirect($this->redirectPath())->with('success', 'Registrasi berhasil, silahkan login');
    }

    public function create($data) {
        $data['password'] = bcrypt($data['password']);
        return \App\Client::create($data);
    }

    public function guard()
    {
        Auth::guard('client');
    }

    public function validator(array $data)
    {
        return Validator::make($data, [
            'nama_client' => 'required',
            'email' => 'required|unique:clients',
            'password' => 'required|confirmed',
            'no_telp' => 'required|string',
            'alamat' => 'required'
        ]);
    }
}
