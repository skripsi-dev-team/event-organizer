<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use Auth;

class ClientController extends Controller
{
    public function index(){
        return view('admin.client.index')->with('clients', Client::all());
    }

    public function destroy($id){
        Client::find($id)->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus data client');
    }


    //Client page
    public function profile(){
        $user = Auth::guard('client')->user();
        $data['user'] = Client::find($user->id);
        return view('client.profile.index', $data);
    }

    public function edit(){
        $user = Auth::guard('client')->user();
        $data['user'] = Client::find($user->id);
        return view('client.profile.edit', $data);
    }

    public function update(Request $request){
        $client = Client::find(Auth::guard('client')->user()->id);
       
        $this->validate($request, [
            'email' => 'required|unique:clients,email,'.$client->id, 
            'nama_client' => 'required', 
            'no_telp' => 'required',
            'alamat' => 'required'
        ]);

        $client->update($request->all());
        
        return redirect()->route('client.profile')->with('success', 'Berhasil mengedit profile');

    }
}
