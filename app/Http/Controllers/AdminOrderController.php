<?php

namespace App\Http\Controllers;

use App\Notifications\AcaraSelesaiNotification;
use App\Notifications\KonfirmasiPembayaranNotification;
use App\Notifications\PesananBatalNotification;
use App\Notifications\PesananSiapNotification;
use Illuminate\Http\Request;
use App\Orders;



class AdminOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.order.index', ['orders' => Orders::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Orders $order)
    {
        return view('admin.order.show', ['order' => $order]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Orders $order)
    {
        if ($order->payments->count() == 0 && $request->status_pesanan != 5) {
            return redirect()->back()->with('error', 'Tidak bisa update status sebelum client melakukan pembayaran!');
        }
        $client = $order->client;

        switch($request->status_pesanan) {
            
            case 2:
                $client->notify(new KonfirmasiPembayaranNotification($client, $order));
            break;
            
            case 3:
                $client->notify(new PesananSiapNotification($client, $order));
            break;
            
            case 4:
                $client->notify(new AcaraSelesaiNotification($client, $order));
            break;

            case 5:
                $client->notify(new PesananBatalNotification($client, $order));
            break;
            
            default:

            break;
        }
    
        $order->status_pesanan = $request->status_pesanan;
        $order->save();

        return redirect()->back()->with('success', 'Status order berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
