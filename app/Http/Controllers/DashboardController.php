<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Orders;
use App\Client;
use Carbon\Carbon;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('chart');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['total_service'] = Service::all()->count();
        $data['total_order'] = Orders::all()->count();
        $data['total_client'] = Client::all()->count();

        $data['selesai'] = Orders::where('status_pesanan', 4)->count();
        $data['progress'] = Orders::whereNotIn('status_pesanan', [0, 4, 5])->count();
        $data['belum_bayar'] = Orders::where('status_pesanan', 0)->count();
    
        return view('admin.dashboard', $data);
    }

    public function chart(Request $request)
    {
        $services = Service::has('orderDetails')->get();

        foreach ($services as $service) {
            $data['services'][] = [
                'service' => $service->nama_service,
                'count'   => $service->orderDetails()->count()
            ];
        }
        unset($services);

        $orders = Orders::whereBetween('tgl_pesanan', [now()->subYear(5)->format('Y-m-d'), now()->format('Y-m-d')])
                        ->orderBy('tgl_pesanan', 'DESC')
                        ->get()
                        ->groupBy(function($val) {
                            return Carbon::parse($val->tgl_pesanan)->format('Y');
                        });

        foreach ($orders as $key => $order) {
            $data['orders'][] = [
                'year'  => $key,
                'count' => $order->count()
            ];
        }
        unset($orders);

        return response()->json([
            'status' => 'ok',
            'data' => $data
        ]);

    }
}
