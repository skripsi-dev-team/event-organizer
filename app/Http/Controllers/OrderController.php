<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Service;
use App\User;
use App\Orders;
use App\Http\Requests\OrderRequest;
use App\Notifications\ClientOrderNotification;
use App\Notifications\AdminOrderNotification;
use Carbon\Carbon;
use Auth;

class OrderController extends Controller
{
    public function placeOrder(OrderRequest $request, Client $client)
    {
        $data = $request->all();
        
        $sameDate = Orders::where('tgl_acara', $data['tgl_acara'])->count();
        
        if ($sameDate >= 3) {
            return redirect()->back()->with('error', 'Tidak bisa membuat order pada tanggal tersebut, coba pada tanggal lain!');
        }
        
        $data['total_harga'] = Service::find($data['service_id'])->harga;
    
        $order = $client->orders()->create($data);
        $order->details()->create([
            'service_id' => $data['service_id']
        ]);

        $client->notify(new ClientOrderNotification($client, $order));
        
        $admin = User::where('email', env('DEFAULT_ADMIN_EMAIL', ''))->first();
        $admin->notify(new AdminOrderNotification($order));

        return redirect()->route('layanan.index')->with('success', 'Pesanan berhasil dibuat');
    }

    public function addOrder(Request $request, Orders $order)
    {
        $check = $order->details()->where('service_id', $request->get('service_id'))->first();
        // if($check){
        //     return redirect()->back()->with('error', 'Tidak dapat memesan service yang sama');
        // }
        $details = $order->details()->create([
            'service_id' => $request->get('service_id')
        ]);

        $service = Service::find($request->get('service_id'));
        $order->total_harga += $service->harga;
        $order->save();

        return redirect()->back()->with('success', '1 Order berhasil ditambahkan');
    }

    public function showPesanan(){
        $client = Auth::guard('client')->user();
        $data['orders'] = Orders::where('client_id', $client->id)->get();

        return view('order.daftar_pesanan', $data);        
    }

    public function detailPesanan($no_order){
        $client = Auth::guard('client')->user();
        $order = Orders::where('client_id', $client->id)
                        ->where('nomor_pesanan', $no_order)
                        ->first();
        if($order == null){
            return redirect()->back()->with('error', 'Data order tidak ada.');
        }

        $data['order'] = $order;
        
        return view('order.detail_pesanan', $data);
    }

    public function deleteService($order_id, $service_id){
        $order = Orders::find($order_id);
        if($order->status_pesanan == 0){
            $sumDetail = $order->details->count();
            if($sumDetail != 1){
                $detail = $order->details->where('service_id', $service_id)->first();
                
                $order->update([
                    'total_harga' => $order->total_harga - $detail->service->harga
                ]);

                $detail->delete();
                
                return redirect()->back()->with('success', 'Berhasil menghapus data layanan');
            } else {
                return redirect()->back()->with('error', 'Gagal menghapus layanan karena hanya ada 1 pesanan');
            }
            
        } else {
            return redirect()->back()->with('error', 'Gagal menghapus layanan karena anda telah melakukan pembayaran');
        }

    }

    
}
