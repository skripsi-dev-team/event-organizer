<?php

use App\Service;
use App\Category;

if (!function_exists('resizeImage')){
    function resizeImage($image){
        $img = Image::make('storage/service_thumbnail/'.$image);
        $img->fit(320, 230, function($constraint){
            $constraint->upsize();
        });

        $img->save('storage/service_thumbnail/small/'.$image);
        return $img;
    }
}

if (!function_exists('resizeImageGallery')){
    function resizeImageGallery($image){
        $img = Image::make('storage/gallery/'.$image);
        $img->fit(320, 200, function($constraint){
            $constraint->upsize();
        });

        $img->save('storage/gallery/small/'.$image);
        return $img;
    }
}

if (!function_exists('hasPerfix')) {
    function hasPrefix(string $prefix) {
        $baseUrl = env('APP_URL', 'http://127.0.0.1:8000');
        $currenUrl = url()->current();

        $sanitize = str_replace($baseUrl, '', $currenUrl);

        if (strpos($sanitize, $prefix) > 0) {
            return true;
        }

        return false;
    }
}


if(!function_exists('showServiceCategory')){
    function showServiceCategory($category_id){
        $service = Service::where('category_id', $category_id)->limit(4)->get();
        return $service;
    }
}

if (!function_exists('makeServiceInisial')) {
    function makeServiceInisial($name) {
        $words = explode(' ', $name);
        $inisial = '';
        foreach ($words as $key => $word) {
            $inisial .= str_split(strtoupper($word))[0];
        }

        return $inisial;
    }
}

if(!function_exists('listCategory')){
    function listCategory(){        
        return Category::has('service')->get();
    }
}

if(!function_exists('generateOrderNumber')) {
    function generateOrderNumber() {
        return 'ODR-'.(string)now()->timestamp;
    }
}

if(!function_exists('userHasUnPaidOrder')) {
    function userHasUnPaidOrder($client) {
        return $client->orders()->where('status_pesanan', 0)->first();
    }
}

if(!function_exists('clientUrlPrefix')){
    function clientUrlPrefix(string $prefix){
        $url = url()->current();
        if (strpos($url, $prefix) > 0) {
            return true;
        }
        return false;
    }
}

if (!function_exists('orderStatusColor')) {
    function orderStatusColor($status) {
        switch ($status) {
            case 0:
                return 'bg-warning';
                break;
            case 1:
                return 'bg-secondary';
                break;
            case 2:
                return 'bg-light text-dark';
                break;
            case 3:
                return 'bg-light text-dark';
                break;
            case 4:
                return 'bg-success';
                break;
            default:
                return 'bg-danger';
                break;
        }
    }
}

if (!function_exists('statusText')) {
    function statusText($status) {
        switch ($status) {
            case 0:
                return 'Menunggu Pembayaran';
                break;
            case 1:
                return 'Menunggu Konfirmasi Pembayaran';
                break;
            case 2:
                return 'Pembayaran Dikonfirmasi';
                break;
            case 3:
                return 'Pesanan Siap Untuk Acara';
                break;
            case 4:
                return 'Acara Selesai';
            default:
                return 'Pesanan Dibatalkan';
                break;
        }
    }
}

?>