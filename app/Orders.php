<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $fillable = [
        'client_id',
        'nomor_pesanan',
        'tgl_pesanan',
        'nama_acara',
        'tgl_acara',
        'alamat_acara',
        'total_harga',
        'catatan',
        'status_pesanan'
    ];

    protected $dates = [
        'tgl_acara'
    ];

    public $statusText = [
        '0' => 'Menunggu Pembayaran',
        '1' => 'Menunggu Konfirmasi Pembayaran',
        '2' => 'Pembayaran Dikonfirmasi',
        '3' => 'Pesanan Siap Untuk Acara',
        '4' => 'Acara Selesai',
        '5' => 'Pesanan Dibatalkan'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function details()
    {
        return $this->hasMany(OrderDetail::class, 'order_id');
    }

    public function getStatusText() {
        return $this->statusText[$this->status_pesanan];
    }

    public function payments() {
        return $this->hasMany(Payment::class, 'order_id');
    }
}
