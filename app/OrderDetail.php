<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = [
        'order_id',
        'service_id'
    ];


    public function order()
    {
        return $this->belongsTo(Orders::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }
}
