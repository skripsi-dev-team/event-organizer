<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'order_id',
        'tgl_pembayaran',
        'nominal_pembayaran',
        'status_pembayaran',
        'bukti_transfer',        
    ];

    protected $dates = [
        'tgl_pembayaran'
    ];

    public function order(){
        return $this->belongsTo(Orders::class);
    }

    public function getStatusText() {
        return ($this->status_pembayaran == 0) ? 'Pembayaran gagal':'Pembayaran berhasil';
    }
}
