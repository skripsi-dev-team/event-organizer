<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Service extends Model
{
    protected $fillable = [
        'nama_service',
        'kode_service',
        'category_id',
        'slug',
        'harga',
        'deskripsi',
        'thumbnail'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class, 'service_id');
    }

    public function setKodeServiceAttribute($val) {
        $padding = 3;
        $index = DB::table('services')->count() + 1;
        $countIdLength = count(str_split((string)$index));
        
        if ($countIdLength < $padding) {
            $padding = $padding - $countIdLength;
        }
        $finalLength = count(str_split(makeServiceInisial($val))) + $padding;

        $this->attributes['kode_service'] = str_pad(makeServiceInisial($val), $finalLength, '0').(string)$index;
    }
}
