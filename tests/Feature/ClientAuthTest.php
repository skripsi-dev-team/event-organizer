<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClientAuthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testClientLoginForm()
    {
        $response = $this->get('/client/login');

        $response->assertStatus(200)
                ->assertViewIs('auth.client.login');
    }

    public function testClientAuthenticated()
    {
        $user = factory(\App\Client::class)->create();
        $response = $this->post('/client/login', [
            'email' => $user->email,
            'password' => 'secret'
        ]);

        $response->assertStatus(302)->assertRedirect('/');
        $this->assertAuthenticated('client');
    }

    public function testRedirectWhenAuthenticateUserAccessLoginForm()
    {
        $user = \App\Client::all()->random();
        $response = $this->actingAs($user, 'client')->get('/client/login');

        $response->assertStatus(302)->assertRedirect('/home');
    }

    public function logout()
    {
        $user = \App\Client::all()->random();
        $response = $this->actingAs($user, 'client')->post('/client/logout');

        $response->assertStatus(302)->assertRedirect('/');
    }

    public function testClientRegisterForm()
    {
        $response = $this->get('/client/register');

        // $response->dump();

        $response->assertStatus(200)->assertViewIs('auth.client.register');
    }

    public function testRegisterClient()
    {
        $sample = factory(\App\Client::class)->make();
        $response = $this->post('/client/register', [
            'nama_client' => $sample->nama_client,
            'email' => $sample->email,
            'password' => 'secret',
            'password_confirmation' => 'secret',
            'no_telp' => $sample->no_telp,
            'alamat' => $sample->alamat
        ]);

        $response->assertStatus(302)->assertRedirect('/client/login')->assertSessionHas('success');
        $this->assertDatabaseHas('clients', $sample->toArray());
    }

    public function testClientCannotAccessRegisterFormWhenAuthenticated()
    {
        $sample = factory(\App\Client::class)->make();

        $response = $this->actingAs($sample, 'client')->get('/client/register');

        $response->assertStatus(302)->assertRedirect('/home');
    }

    public function testClientCannotRegisterIfDuplicateEmail()
    {
        $sample = factory(\App\Client::class)->make([
            'email' => 'matteo97@example.org'
        ]);
        $response = $this->post('/client/register', [
            'nama_client' => $sample->nama_client,
            'email' => $sample->email,
            'password' => 'secret',
            'password_confirmation' => 'secret',
            'no_telp' => $sample->no_telp,
            'alamat' => $sample->alamat
        ]);

        $response->dump();

        $response->assertStatus(302)->assertSessionHasErrors(['email']);
        $this->assertDatabaseMissing('clients', $sample->toArray());
    }
}
