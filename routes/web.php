<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $data['categories'] = App\Category::where('nama_kategori', '!=', 'paket')->limit(6)->get();
    $data['services'] = App\Category::where('nama_kategori', 'paket')->first()->service;
    return view('welcome', $data);
});

//Admin Route
Route::prefix('admin')->group(function(){
    
    Auth::routes();
    
    Route::middleware('auth:web')->group(function() {
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');
        //master
        Route::prefix('master')->group(function() {
            Route::resource('kategori', 'CategoryController');
            Route::resource('service', 'ServiceController');
            Route::resource('user', 'AdminController');
            Route::resource('gallery', 'GalleryController');
            Route::get('client', 'ClientController@index')->name('client.admin.index');
            Route::delete('client/{id}/delete', 'ClientController@destroy')->name('client.admin.destroy');
        });

        Route::prefix('transaksi')->group(function(){
            Route::resource('order', 'AdminOrderController');
            
            Route::delete('payment/{id}/delete/', 'PaymentController@delete')->name('payment.delete');
        });


        Route::get('laporan', function(){
            return redirect()->back();
        });
        
        Route::get('laporan/pesanan', 'LaporanController@pesanan')->name('laporan.pesanan');
        Route::get('laporan/pesanan/print', 'LaporanController@printPesanan');

        Route::get('laporan/pembayaran', 'LaporanController@pembayaran')->name('laporan.pembayaran');
        Route::get('laporan/pembayaran/print', 'LaporanController@printPembayaran');
    
    });
});

Route::get('admin', function(){
    return redirect()->route('dashboard');
});

Route::get('admin/register', function(){
    return redirect()->route('login');
});



//Client Auth
Route::prefix('client')->group(function() {
    Route::get('login', 'ClientLoginController@showLoginForm')->name('client.loginform');
    Route::post('login', 'ClientLoginController@login')->name('client.login');
    Route::post('logout', 'ClientLoginController@logout')->name('client.logout');
    Route::get('register', 'ClientRegisterController@showRegisterForm')->name('client.registerform');
    Route::post('register', 'ClientRegisterController@register')->name('client.register');

    Route::middleware('auth-client:client')->group(function(){
        Route::get('profile', 'ClientController@profile')->name('client.profile');
        Route::get('profile/edit', 'ClientController@edit')->name('client.edit');
        Route::put('profile/edit', 'ClientController@update')->name('client.update');
    });
});

//Clint Order
Route::middleware('auth-client:client')->group(function() {
    Route::post('clients/{client}/place-order', 'OrderController@placeOrder')->name('client.order');
    Route::post('order/{order}/add', 'OrderController@addOrder')->name('order.add');
    Route::get('pesanan-saya', 'OrderController@showPesanan')->name('client.pesanan-saya');
    Route::get('pesanan-saya/{order}/detail', 'OrderController@detailPesanan')->name('client.pesanan.detail');
    Route::delete('clients/{order_id}/{service_id}/delete', 'OrderController@deleteService')->name('client.service.delete');
    //payment
    Route::get('pesanan-saya/{order}/bayar', 'PaymentController@formBayar')->name('client.pesanan.bayar');
    Route::post('clients/bayar', 'PaymentController@bayar')->name('client.pembayaran');
});


//Listing Product
Route::prefix('layanan')->group(function(){
    Route::get('/', 'ListingController@indexService')->name('layanan.index');
    Route::get('{slug}', 'ListingController@showService')->name('layanan.service');
    Route::get('{slug_kategory}/{slug_service}', 'ListingController@detailService')->name('layanan.detail');
});

//Gallery 
Route::get('gallery', 'ListingController@gallery');


Route::get('home', function(){
    return view('client.app');
});

